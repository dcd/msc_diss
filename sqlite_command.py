import sqlite3
from os.path import exists


def runCommand(dbpath,com):
    if exists(dbpath):
        globalDBconn=sqlite3.connect(cache_dbpath)
        globalDBconn.text_factory=str
        c=globalDBconn.cursor()
        c.execute(com)
        globalDBconn.commit()
        c.close()
        globalDBconn.close()

cache_dir="c:\\NLP\\thesis\\cache\\"
cache_dbpath = cache_dir+ "cache.db"

runCommand(cache_dbpath,"delete from all_triples where subj ='http://dbpedia.org/resource/George_Frideric_Handel'")