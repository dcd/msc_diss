#-------------------------------------------------------------------------------
# Name:		bl_generate
# Purpose:	  Baseline generation
#
# Author:	  Daniel Duma
#
# Created:	 07/06/2012
# Copyright:   (c) Daniel Duma 2012
# Licence:	 <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from util import *
from rdf import *
import cmath

class iterFill:
	def __init__(self):
		self.varnum=0
		self.graphPaths=[]
		self.values=[]

class generateRec:
	def __init__(self,wiki_link):
		self.discourse_model=defaultdict(int)
		self.full_subj=fullTripleSubject(wiki_link)
		self.wiki_link=wiki_link
		self.classes=[]
		self.basicRE=getBasicSelfRE(wiki_link) # basic self- referring expression, i.e. rdfs_label/foaf:name
		self.triples=[]
		self.triples_used=defaultdict(int)
		self.gp_values={}
		self.found_gps=[]
		self.trained_model=None
	def __repr__(self):
		res="<generateRec (wiki_link:"+self.wiki_link+")>"
		return res

##def getPoolValue(gen_model,pool):
##	for gp in pool:
##		if gen_model.gp_values.has_key(gp):
##			return gen_model.gp_values[gp]

def getLoadedGraphPathValue(gen_model, gp):
	if gp in gen_model.gp_values:
		return gen_model.gp_values[gp]
	else:
		return []

def getGraphPathValue(graphPath,self_value):
	"""
	resolve a graphPath (a string like $self\bla1\bla2) into a value recursively
	automatically replaces $self with actual triple subject

	in -> triple = string, self_value = value to set $self mask to
	out -> list of rdfTriple
	"""
	graphPath=expandStringNamespaces(graphPath)
	bits=graphPath.replace(self_pointer,self_value)
	bits=bits.split(gp_sep)
	res=""

	if len(bits) == 1:
		bits.append(self_REG_default)
##	else:
##		if bits[-1]

	res=quickTripleQuery(bits[0],bits[1])
	if len(res) == 0:
		return []

	if len(bits) > 2:
		for cnt in range(2,len(bits)):
			val=fullTripleSubject(wikifyTitle(res[0].value))
			res=quickTripleQuery(val,bits[cnt])
			if len(res) == 0:
				return []

	return res

def recursiveIterate(template,iters,pos,varnum=0):
	res=[]
	if pos == len(iters): return []

	for val in iters[pos].values:
		varnum+=1
		tmp=template.replace("$[x"+str(pos+1)+"]$",str(val))
		if pos < len(iters) -1:
			res.extend(recursiveIterate(tmp,iters,pos+1,varnum))
		else:
			res.append(tmp)

	return res


def getPathTrail(tp):
	pos=tp.rfind(gp_sep)
	if pos == -1: pos=0
	return tp[pos+1:]

def splitPathTrail(tp):
	pos=tp.rfind(gp_sep)
	if pos == -1: pos=0
	return tp[pos+1:],tp[:pos]


def filledTemplateToString(temp):
	res=u""
	cnt=1
	for t in temp:
		if isinstance(t,basestring):
			res+=t+" "
		if isinstance(t,dict):
			res+=u"$[x"+unicode(cnt)+u"]$ "
			cnt+=1
	return res


def fillInTemplateSlots(gen_model,template,self_link):
	filled_template=[]
	varnum=0

	all_graphPaths=defaultdict(int)
	found_values=defaultdict(int)
	missing_values=[]

	for token in template:
		if isinstance(token,list):
			varnum+=1

			filled_val=defaultdict(list)
			for graphPath in token:
				assert isinstance(graphPath,basestring),"cuaaaaaac"
				path=graphPath
				all_graphPaths[path]+=1

				if path==self_pointer:
					filled_val[self_pointer].append((path,token.surface))
					found_values[varnum]+=1
				else:
					results=getLoadedGraphPathValue(gen_model,path)
					for value in results:
						filled_val[value].append((path,token.surface))
					if len(results) > 0:
						found_values[varnum]+=1

			filled_template.append(filled_val)
			if found_values[varnum]==0:
				missing_values.append(varnum)

		if isinstance(token,basestring):
			filled_template.append(token)

	return filled_template, all_graphPaths, found_values, varnum, missing_values


def fillInTemplateSlotsV2(gen_model,template,self_link):
	filled_template=[]
	varnum=0

	all_graphPaths=defaultdict(int)
	found_values=defaultdict(int)
	missing_values=[]

	for token in template:
		if isinstance(token,list):
			varnum+=1

			filled_val=defaultdict(list)
			for graphPath in token:
				assert isinstance(graphPath,graphPath),"cuaaaaaac"
				trail,path=splitGraphPathMod(graphPath)
				all_graphPaths[path]+=1

				if path==self_pointer:
					filled_val[self_pointer].append((path,trail))
					found_values[varnum]+=1
				else:
					results=getLoadedGraphPathValue(gen_model,path)
					for value in results:
						filled_val[value].append((path,trail))
					if len(results) > 0:
						found_values[varnum]+=1

			filled_template.append(filled_val)
			if found_values[varnum]==0:
				missing_values.append(varnum)

		if isinstance(token,basestring):
			filled_template.append(token)

	return filled_template, all_graphPaths, found_values, varnum, missing_values


def generateAllSurfaceRealisations(template,self_link,self_basic_RE):
	iters=[]

	strtemplate=filledTemplateToString(template)

	num=0
	for token in template:
		if isinstance(token,dict):
			it=iterFill()
			for val in token.values():
				it.graphPaths.extend(val)
			it.values.extend(token.keys())
			num+=1
			it.varnum=num
			iters.append(it)

	for it in iters:
		if len(it.values) == 0: # if failed to find a value, drop entire generation
			print "No value found for: ",it.varnum, it.graphPaths
			print filledTemplateToString(template)
			return []

	surface=recursiveIterate(strtemplate,iters,0)

	return surface


def getSentenceTokenDensity(sp):
	return len([x for x in sp if isinstance(x,list)]) / float(len(sp))


def informationNewness():
	pass

def chooseBestSRforSlot(candidates):
	if len(candidates) == 0: return "",[]
	scores=[]
	for c in candidates:
		pun=0
		if re.match(r"[.,]",c[0]):pun+=1
		score=len(c[0])*pow(2,pun)
		scores.append((c,score))
	a,b=sorted(scores,key=lambda x:x[1],reverse=False)[0]
##	print "Of candidates",candidates
##	print "The chosen is",a[0],", from predicates", a[1]

	return a

def getGenderFromSparql(wiki_link):
	res=[]
	query="""
SELECT ?obj
 WHERE {
 <%s> <%s> ?obj .

}
 """ % (fullTripleSubject(wiki_link),"http://xmlns.com/foaf/0.1/gender")
#FILTER (langMatches(lang(?obj),"") || langMatches(lang(?obj),"en") )
	data=""
	while data=="":
		data=sparqlQuery(query, "http://localhost:8080/openrdf-sesame/repositories/dbpedia")

	if isinstance(data,basestring):
		print data
		return ""

	d =data.items()[1][1]['bindings'][0]
	obj=d.items()[0][1]
	if obj["type"]=="literal":
		if "xml:lang" in obj:
			lng=obj["xml:lang"]
		else:
			lng=""
	else:
		if "xml:lang" in obj:
			lng=obj["xml:lang"]
		else:
			lng=""


	return obj["value"]

numToMonth={1:"January",2:"February",3:"March",4:"April",5:"May",6:"June",
7:"July",8:"August",9:"September",10:"October",11:"November",12:"December"}

def surfaceDate(date):
	"""
	Return a nice-looking date from a standard YYYY-MM-DD format
	"""
	yy=int(date[:4])
	mm=int(date[5:7])
	dd=int(date[8:10])

	return str(dd)+" "+numToMonth[mm]+" "+str(yy)

def generateGreedyBestSurfaceRealisation(template, gen_model):
	strtemplate=filledTemplateToString(template)
	used=defaultdict(int)

	res=""
	num=0
	class_model=gen_model.trained_model
	self_pronoun=class_model.bestPronoun()

	for token in template:
		if isinstance(token,dict):
##			print "Token:",token
			if self_pointer in token.keys():
				if self_pointer in gen_model.discourse_model:
					if token[self_pointer][0][1]==token_gen:
						text=posessives[self_pronoun][0]
					elif token[self_pointer][0][1]==token_nom:
						text=self_pronoun
				else:
					gen_model.discourse_model[self_pointer]+=1
					text=gen_model.basicRE
					if token[self_pointer][0][1]==token_gen:
						text+="'s"
			else:
				candidates=[]
				for val in token.keys():
					text=""
					if token[val][0][1]==token_gen:
						text=val+"'s"
					else:
						text=val

					candidate_used=[tup[0] for tup in token[val]]
					candidates.append((text,candidate_used))
					pass # end of for every val in token

				text,used_triples=chooseBestSRforSlot(candidates)

				for k in used_triples:
					gen_model.triples_used[k]+=1
##				gen_model.triples_used.update(used_triples)
				pass # end of else: self_pointer not in token.keys()
			res+=text+" "
			pass # end of if isinstance

		if isinstance(token,basestring):
			res+=token+" "

	res+="."
	res=res.strip()
	res=res[0].upper()+res[1:]
	res=re.sub(r"\s+([.,;])",r"\1",res)
	res=re.sub(r"\(\s(.*)\s\)",r"(\1)",res)

	return res



def sentenceFulfillsRequirements(gen_model,s):
	"""
	Return True if
	"""
	for t in s:
		if isinstance(t,templateSlot):
			full=False
			for path in t:
				# TODO: add stuff to deal with pools
				for p in gen_model.trained_model.pools:
					if str(path) in p:
						full=True
						break
			if not full:
				return False
	return True

def enoughParametersForSentence(available_paths,s):
	import copy
	paths_copy=copy.deepcopy(available_paths)
	for t in s:
		if isinstance(t,list):
			full=False
			for path in t:
				if paths_copy[path] > 0:
					paths_copy[path]-=1
					full=True
			if not full:
				return False
	return True


def getClassTriplesForModel(gen_model):
	gen_model.triples=quickTripleQuery(gen_model.full_subj,rdf_type,self_pointer)
	gen_model.classes=[t.value for t in gen_model.triples]


def getTriplesForModel(gen_model):
##	model.triples=getTriplesUsingCache(wiki_link)
##	model.triples=overrideTriplesSubject(selectTriplesLanguage(model.triples,"EN"),self_pointer)
##	model.triples=shortenTripleNamespaces(model.triples)
	for gp in gen_model.trained_model.ngrams[1]:
		gp=gp[0]
		found_triples=getGraphPathValue(gp,gen_model.full_subj)
##		trail, path=splitPathTrail(gp)
		if len(found_triples) > 0:
			lst=gen_model.gp_values.setdefault(gp,[])
			for v in found_triples:
				lst.append(v.value)

def getTriplesForModelPools(gen_model):
	for pool in gen_model.trained_model.pools:
		found_triples=[]
		for gp in pool:
			found_triples=getGraphPathValue(gp,gen_model.full_subj)
	##		trail, path=splitPathTrail(gp)
			if len(found_triples) > 0:
				pool.values.extend([v.value for v in found_triples])
##				for gp2 in pool:
##					lst=gen_model.gp_values.setdefault(gp2,[])
##     				for v in found_triples:
##						lst.append(v.value)
				break


def selectClassToGenerateFrom(gen_model,classModels):
	words=defaultdict(int)
	classes=[]
	for c in gen_model.classes:
		s=space_out_camel_case(extractClassName(c)).split()
		s=removeStopWords(s)
		for w in s:
			words[w]+=1
		classes.append((c,s))

	scores=[]
	for tp in classes:
		score=0
		for word in tp[1]:
			score+=words[word]
		scores.append((tp[0],score))


	for c,score in sorted(scores,key=lambda x:x[1],reverse=True):
##		print c,score
		if c in classModels:
			return c

	return ""

##		if c in classModels:
##			res.append[c]
##		else:
##			print "No trained model found for class",c

	return res

def recursiveBuildSentenceList(chart,cur_list,available_paths,pos):
	res=[]
	if pos == len(chart): return []

	for val in chart[pos:]:
		tmp=cur_list[:]
		if val not in tmp:
			if enoughParametersForSentence(available_paths,val):
				tmp.append(val)

		if pos < len(cur_list) -1:
			res.extend(recursiveBuildSentenceList(chart,cur_list,available_paths,pos+1))
		else:
			res.append(tmp)

	return res

def ngramScore(combi,gen_model):
	ngramlist=[[""],[""],[""]]
	for s in combi:
		for token in s:
			if isinstance(token,list):
				ngramlist.append(token)

	total_score=1
	for x,n in enumerate(ngramlist[3:]):
##		for i in range(1,maxNgram+1):
			i=maxNgram
			y=x+3+1
			nglist=iterateBuildNgrams(ngramlist[y-i:y],0)
			highest=0
			for ng in nglist:
				ng=tuple(ng)
				if gen_model.trained_model.ngrams[i][ng]> highest:
					highest=gen_model.trained_model.ngrams[i][ng]
			total_score*=highest
	return total_score


def generateArticle(classModels,wiki_link,article_class=None):
	"""
	generate the text for one article identified by (wiki_link)

	"""
	gen_model=generateRec(wiki_link)
	getClassTriplesForModel(gen_model)

	if article_class is not None:
		if article_class not in classModels:
			print "Cannot find article class",article_class
			return
		classModel=classModels[article_class]
	else:
		cls=selectClassToGenerateFrom(gen_model,classModels)
		if cls=="":
			print "Cannot find a single class to generate from: ",gen_model.classes
##			assert 1==0
			return
		else:
			classModel=classModels[cls]
			print "Using model:",cls

	gen_model.trained_model=classModel

	getTriplesForModelPools(gen_model)

	chart=[]

	full_self_link=fullTripleSubject(wiki_link)

	for s in gen_model.trained_model.sentences:
		if sentenceFulfillsRequirements(gen_model,s):
			chart.append(s)

	cur_list=[]
	available_paths=defaultdict(int)
	for gp in gen_model.gp_values:
		available_paths[gp]+=1

	possibleCombinations=recursiveBuildSentenceList(chart,cur_list,available_paths,0)

	bestlist=[]
	for p in possibleCombinations:
		if len(p) > 0:
			avgDensity=sum([getSentenceTokenDensity(s) for s in p]) / float(len(p))
			comb_score=avgDensity*ngramScore(p,gen_model)
			bestlist.append((p,comb_score))

	for candi,score in sorted(bestlist,key=lambda x:x[1],reverse=True):
		for s in candi:
			filled,all_paths,found_values,varnum, missing_values=fillInTemplateSlots(gen_model, s,full_self_link)

			if len(found_values) < varnum:
				print "Couldn't find values for all the slots for template:"
				print candidate.toStringTemplate()
				for m in missing_values:
					print "Missing value for slot ",m
				continue

			text=generateGreedyBestSurfaceRealisation(filled,gen_model)
			print text,

def loadModelDict(filename):
	f=open(filename,"r")
	res=cPickle.load(f)
	return res

def getLabelFor(uri):
	labels=quickTripleQuery(uri,rdfs_label)
	for l in labels:
##			print l
		if l.dataType != "uri" and (l.lang == "" or l.lang.lower() == "en") :
			return l.value

	res=space_out_camel_case(extractClassName(uri))
	return res

def pprintTriplesFor(classModels,wiki_link):
	gen_model=generateRec(wiki_link)
	getClassTriplesForModel(gen_model)

	cls=selectClassToGenerateFrom(gen_model,classModels)
	if cls=="":
		print "Cannot find a single class to generate from: ",gen_model.classes
##			assert 1==0
		return
	else:
		classModel=classModels[cls]
		print "Using model:",cls

	gen_model.trained_model=classModel

	getTriplesForModelPools(gen_model)
	print "\n\n"
	print "Category:",getLabelFor(cls)
	for p in gen_model.trained_model.pools:
		if len(p.values) > 0:
			pred=extractPredFromGP(p[0])
			if pred not in [rdf_type] and pred not in useless_predicates:
				print getLabelFor(pred), "=",
				for val in p.values:
					surface=val
					if is_URI(val):
						surface=getLabelFor(val)
					print surface+",",
				print

	print "Sentences templates:"
	for s in gen_model.trained_model.sentences:
		print s
##	assert 1==0
	pass


model_filename="c:\\nlp\\thesis\\bl_model2.mod"

print "Using model: ",model_filename

classModels=loadModelDict(model_filename)
print "Generating article:"
##generateArticle(classModels,"George_Frideric_Handel","http://dbpedia.org/class/yago/GermanComposers")
##generateArticle(classModels,"Johann_Sebastian_Bach","http://dbpedia.org/class/yago/GermanComposers")
##generateArticle(classModels,"Johann_Sebastian_Bach")
##generateArticle(classModels,"Metallica")
##pprintTriplesFor(classModels,"Johann_Sebastian_Bach")
##pprintTriplesFor(classModels,"Metallica")
pprintTriplesFor(classModels,"Lionel_Messi")

##generateArticle(classModels,"Wolfgang_Amadeus_Mozart")
##generateArticle(classModels,"Joe_Bloggs","http://dbpedia.org/class/yago/GermanComposers")
##generateArticle(classModels,"Spain","http://dbpedia.org/class/yago/Countries")
##print getGenderFromSparql("Johann_Sebastian_Bach")
