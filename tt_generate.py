#-------------------------------------------------------------------------------
# Name:		tt_generate
# Purpose:	  TripleText-like generation
#
# Author:	  Daniel Duma
#
# Created:	 18/07/2012
# Copyright:   (c) Daniel Duma 2012
# Licence:	 <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from util import *
from rdf import *
import re
import cmath
from genderSupport import *

MAX_TT_CLASES=5

class tt_generateRec:
	def __init__(self,wiki_link):
		self.discourse_model=defaultdict(int)
		self.full_subj=fullTripleSubject(wiki_link)
		self.wiki_link=wiki_link
		self.classes=[]
		self.basicRE=getBasicSelfRE(wiki_link) # basic self- referring expression, i.e. rdfs_label/foaf:name
		self.triples=[]
		self.triples_used=defaultdict(int)
		self.gp_values={}
		self.found_gps=[]
		self.trained_model=None

def tt_stringHash(s):
	s=re.sub(r"[.,\-_\\/]"," ",s)
	words=s.split()
	words=list(set(words))
	words=sorted(words)
	h=" ".join(words)
	return h

def tt_excludeDuplicates2(candidates):
	"""
	in: candidates = list of (string,predicate)
	"""
	hashes={}
	keep=[]
	for c in candidates:
		h=tt_stringHash(c)
		if h not in hashes:
			hashes[h]=[]
		if c not in hashes[h]:
			hashes[h].append(c)

	for h in hashes:
		keep.append(tt_chooseBestSRforSlot(hashes[h]))
	return keep

def tt_excludeDuplicates(candidates):
	"""
	in: candidates = list of (string,predicate)
	"""
	hashes=[]
	keep=[]
	for c in candidates:
		h=tt_stringHash(c)
		if h not in hashes:
			hashes.append(h)
			keep.append(c)
	return keep

def tt_chooseBestSRforSlot(candidates):
	if len(candidates) == 0: return "",[]
	scores=[]

	for c in candidates:
		pun=0
		if re.match(r"[.,]",c[0]):pun+=1
		score=len(c[0])*pow(2,pun)
		scores.append((c,score))
	a,b=sorted(scores,key=lambda x:x[1],reverse=False)[0]
##	print "Of candidates",candidates
##	print "The chosen is",a[0],", from predicates", a[1]

	return a

def tt_getClassTriplesForModel(gen_model):
	gen_model.triples=quickTripleQuery(gen_model.full_subj,rdf_type,self_pointer)
	gen_model.classes=[t.value for t in gen_model.triples]


def tt_makeSentence(subj, pred, obj):
	res=subj+" "+pred+" "+codecs.decode(obj, "utf-8")+"."
	res=res[0].upper()+res[1:]
	res=re.sub(r"\s(a)\s([aeiouAEIOU])",r" an \2",res)
	return res

def tt_getTriplesForModel(gen_model):
##	model.triples=getTriplesUsingCache(wiki_link)
##	model.triples=overrideTriplesSubject(selectTriplesLanguage(model.triples,"EN"),self_pointer)
##	model.triples=shortenTripleNamespaces(model.triples)
	triples=filterOutUselessTriples(getTriplesUsingCache(gen_model.wiki_link))
	triples=selectTriplesLanguage(triples,"en")
	res=defaultdict(list)

	for t in triples:
		sameas=quickTripleQuery(t.pred,owl_sameas)
		sameas.extend(quickTripleQuery(t.pred,owl_equivalent))
		skip=False
		for same in sameas:
			if same.value in res:
				skip=True
				print "Found equivalent predicate:", t.pred,"=",same.value
				break
		if skip: break

		if t.pred not in [rdf_type]:
			res[t.pred].append(t.value)
	return res

def tt_selectBestClass(gen_model):
	words=defaultdict(int)
	classes=[]
	for c in gen_model.classes:
		s=getLabelFor(c).split() # retrieve rdfs:label instead
		numwords=len(s)
##		print " ".join(s).strip()
		s=removeStopWords(s)

		for w in s:
			words[w]+=1
		classes.append((c,s,numwords)) # save the class, its

	scores=[]
	for tp in classes:
		score=0.0
		for word in tp[1]:
			score+=words[word]
		divide=float(tp[2])
		if tp[2]==1:
			divide=5
		score/=divide # normalise the score by the word length
		scores.append((tp[0],score))

	if len(scores) > 0:
		return sorted(scores,key=lambda x:x[1],reverse=True)[:MAX_TT_CLASES]
	return []


def tt_stringFromList(lst):
	if len(lst) == 0: return ""
	if len(lst) == 1: return lst[0]

	res=""

	for i,e in enumerate(lst):
		res+=e
		if i == len(lst)-2:
			res+=" and "
		elif i== len(lst)-1:
			pass
		else:
			res+=", "
	return res



def tt_generateArticle(wiki_link):
	"""
	generate the text for one article identified by (wiki_link) using a
	Triple-Text approach

	"""
	print "\nGenerating article:"
	gen_model=tt_generateRec(wiki_link)
	tt_getClassTriplesForModel(gen_model)
	used_predicates=[]

	cls=tt_selectBestClass(gen_model)
	if len(cls)==0:
		print "Cannot find a single class to generate from: ",gen_model.classes
##			assert 1==0
		return
	print cls
	best_class_label=makePluralSingular(getLabelFor(cls[0][0]))

	self_label=getFoafNameOrLabelFor(gen_model.full_subj)
	if isinstance(self_label,list): self_label=self_label[0]
	triples=tt_getTriplesForModel(gen_model)

##	if len(cls) > 1:
##		print tt_makeSentence(self_label,"is a",best_class_label+" and a "+getLabelFor(cls[0][0])),
##	else:
	print tt_makeSentence(self_label,"is a",best_class_label),

	gender=getGenderByAllMeans(gen_model.full_subj)

	if gender=="male":pronoun="he"
	elif gender=="female":pronoun="she"
	else:
		pronoun="it"

	for i,t in enumerate(triples):
		pred=getLabelFor(t).lower()
		if t != self_label and extractClassName(t) not in used_predicates:

			values=[]
			if len(triples[t]) == 1:
				verb="is"
				obj=triples[t][0]
			else:
				verb="are"
				pred=makeSingularPlural(pred)

			for val in triples[t]:
				if is_URI(val):
					label=getLabelFor(val)
					if label=="": label=val
					values.append(label)
				elif is_rdf_date(val):
##					print "is date!!!!!"
					values.append(surfaceDate(val))
				else:
					values.append(val)

			values=tt_excludeDuplicates2(values)

			if isHexNumber(values[0]):
				continue

			if values[0][0]=="_":
				continue

			if len(values[0].split()) > 10: # if the firstvalue has more than x words, skip the triple
				continue

			if isNumberAndIsLessThan(values[0],small_value_threshold):
				continue

			if values[0][-4:] in image_file_extensions:
				continue

			if pred.startswith("is") or pred.startswith("has"):
				words=pred.split()
				part2=" ".join(words[1:]).strip()
				part1=words[0]+" a "
				pred=part1+part2
				verb=":"
				subj=pronoun
			elif pred.startswith("was"):
				subj=pronoun
				verb=""
			elif pred.startswith("known"):
				subj=pronoun
				verb="is"
			else:
				subj=posessives[pronoun][0]

			used_predicates.append(extractClassName(t))
			obj=tt_stringFromList(values)
			print tt_makeSentence(subj+" "+pred,verb,obj),


##tt_generateArticle("Jennifer_Saunders")
##tt_generateArticle("Hyundai_Motor_Company")
##tt_generateArticle("Nicole_Scherzinger")
##tt_generateArticle("Morocco")
##tt_generateArticle("Woody_Woodpecker")
##tt_generateArticle("Real_Zaragoza")
##
##tt_generateArticle("Fernando_Gago")
##tt_generateArticle("American_Idol")
##tt_generateArticle("Mercyful_Fate")
tt_generateArticle("William_Herschel")
##tt_generateArticle("Belgrade")
##tt_generateArticle("Winx_Club")
