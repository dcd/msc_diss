﻿#-------------------------------------------------------------------------------
# Name:		alignments
# Purpose:
#
# Author:	  Daniel Duma
#
# Created:	 03/04/2012
# Copyright:   (c) Daniel Duma 2012
# Licence:	 LGPL v3
#-------------------------------------------------------------------------------
#!/usr/bin/env python

import logging
from time import strptime,strftime
from rdf import *
from util import *
import codecs

logging.basicConfig(level=logging.DEBUG)

def generateSL(value,knownFormat=None):
	""" Take a value string (object of RDF triple) and return a list of surface realisations (SLs) for it
	"""
	res=[]
##	print "generate SL for: ", value, knownFormat
##	knownFormat=str(knownFormat)

	vFormatPos=value.rfind("^^<http://www.w3.org/2001/XMLSchema#")
	if vFormatPos > -1:
		vFormat=value[vFormatPos+2:]
		logging.debug("found a format string: %s. TO DO: implement handler" % vFormat)

		if knownFormat is None:
			if vFormat in ["http://www.w3.org/2001/XMLSchema#double",
			"http://www.w3.org/2001/XMLSchema#float","http://www.w3.org/2001/XMLSchema#decimal",
            "http://www.w3.org/2001/XMLSchema#integer"]:
				knownFormat="number"

		value=value[:vFormatPos]

	value=value.strip("\"' ")

	if knownFormat is None:
		if is_URI(value):
			knownFormat="uri"
		elif is_number(value):
			knownFormat="typed-literal"
		else:   # must be a string
			knownFormat="literal"

	if knownFormat=="literal":
		res.append(value)

	if knownFormat=="uri":
		if value.startswith("http://dbpedia.org/"):
	##		print "it's a URI!",value
			labels=quickTripleQuery(value,rdfs_label)
			for l in labels:
	##			print l
				if l.dataType != "uri" and (l.lang == "" or l.lang.lower() == "en") :
					res.append(l.value)
##					print "found label:",l.value
		else:
			res.append(value)
##			if
##			res.append(space_out_camel_case(extractClassName(value)).lower())
	##	print value

	if knownFormat in ["typed-literal", "date"]:
		if re.match("(1|2|3)[0-9]{3}[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])",str(value)):
##			res.append("$date(%s)"%value)
			res.append(value)
		else:
			res.append(value)

	if knownFormat == "number":
		try:
			num=float(value)
			res.append(num)
		except:
			pass

##{u'datatype': u'http://www.w3.org/2001/XMLSchema#date',
## u'type': u'typed-literal',
## u'value': u'1759-04-14'}

##	for r in res:
##		print r

	return res, knownFormat

def graphPathFromTriple(triple):
	"""
	in -> tiple = rdfTriple
	out -> string of triple.subj + "\" + triple.pred
	"""
	return triple.subj+gp_sep+triple.pred

def generateSLtokens(triples):
	res=[]
	for t in triples:
		# ideally, the dataType of the triple would allow to generate surface realisations, but
		# we get "literal" for URIs
##		sl=generateSL(t.value,t.dataType)
		sl,knownFormat=generateSL(t.value)
		if t.pred in [rdf_type,short_rdf_type]:
			spaced=selectTriplesLanguage(quickTripleQuery(t.pred,rdfs_label), "EN")
			if len(spaced) == 0:
				spaced=[space_out_camel_case(extractClassName(t.value)).lower()]
			for sp in spaced:
				sl.append(makePluralSingular(sp))
##			sl.append(space_out_camel_case(extractClassName(t.value)).lower())
		for s in sl:
##			res.append((graphPathFromTriple(t),s,t.value))
##			dt=""
##			if t.dataType == xsd_date: dt="date"
##			if t.dataType == xsd_date: dt="date"
			if knownFormat != "number": knownFormat="literal"
			res.append((graphPathFromTriple(t),s,knownFormat)) # TODO: dataType should be used for NER
	return res

def spotLinkedEntitiesInText(entities,text_tokens):
	"""
	in-> entities = list of tokens (value_uri,pred), text_tokens = list of tuples (word,link)
	out-> list of entities (value_uri,pred)
	"""
	res=[]

	for e in entities:
		for t in text_tokens:
			if fullTripleSubject(t[1])==e[0]:
				res.append(e)

	return res

def spotAlignments(tokens,text_tokens):
	""" tokens is a list of tuples (pred,word) text is a list of tuples (word,link)
	returns a list of tuples ( (pred,word), pos)"""
	res=[]
	for i,w in enumerate(text_tokens):
		for t in tokens:

			pt1=t[1]
			if not containsUpper(t[1]):
				pt2=w[0].lower()
			else:   # if value has upper case, the token must match the case
				pt2=w[0]
			pt1=re.sub(r"[^\w]"," ",pt1)
			pt2=re.sub(r"[^\w]"," ",pt2)
			if pt1==pt2: # flexible gazeteer? edit distance-ish?
				res.append((t,i))
	return res

def countOccurrences(sub,string):
	"""
	adapted from http://stackoverflow.com/questions/2970520/string-count-with-overlapping-occurances
	"""
	count = 0
	start = 0
	try:
		while True:
			start = string.find(sub, start) + 1
			if start > 0:
				count+=1
			else:
				return count
	except:
		return 0

def overlapScore(str1,str2):
	str1=str1.lower().split()
	str2=" "+str2.lower()+" "
	score=0
	weights={1:0.2,2:0.4,3:0.6,4:0.8}

	for n in range (1,5,1): # 1 to 4 n-grams
		hits=0
		for cnt in range(0,len(str1)-(n-1)):
			hits+=countOccurrences(" "+" ".join(str1[cnt:cnt+n]).strip()+" ",str2)
		score+=weights[n]*hits

	score/=len(str1)
	return score

def spotAlignmentsWithLinks(tokens,text_tokens):
	""" tokens is a list of tuples (pred,word) text is a list of tuples (word,link)
	returns a list of tuples ( (pred,word), pos)"""
	res=[]
	for i,w in enumerate(text_tokens):
		for t in tokens:
			if fullTripleSubject(w[1]).lower()==t[1].lower(): # if the link ==s the searched token
				res.append((t,i))
			# if the surface realisation matches the link
			pt1=re.sub(r"[^\w]"," ",w[0].lower())
			pt2=re.sub(r"[^\w]"," ",t[1].lower())
			if pt1==pt2: # flexible gazeteer? edit distance-ish?
				res.append((t,i))
	return res

def containsUpper(s):
	for c in s:
		if c.isupper():
			return True
	return False

def spotAlignmentsAsATlist(tokens,text_tokens):
	"""
	in: tokens = list of tuples (pred,word), text_tokens = list of tuples (word,link)
	out: a list of alignedToken
	"""

	def samePredicates(t1,t2):
		l1=t1.spottedTriples
		l2=t2.spottedTriples

		if len(l1) != len (l2):
			return False
		for p in l1:
			if p not in l2:
				return False
		return True

	res=[]
	for i,w in enumerate(text_tokens):
		at=alignedToken()
		at.text=w[0]
		at.link=w[1]
		at.spottedTriples=[]

		for t in tokens:
			# COMPARISON: ADD REGULAR EXPRESSIONS HERE
			t_text=t[1]
			t_pred=t[0]
			t_dataType=extractClassName(t[2])
			if t[2]=="uri": # if dataType is uri
				if w[1].lower()==t[1].lower(): # if the link == the token
					at.spottedTriples.append(t[0])
			elif t[2]=="literal":
				# if the surface realisation matches the link
				pt1=t[1]
				if not containsUpper(t[1]):
					pt2=w[0].lower()
				else:   # if value has upper case, the token must match the case
					pt2=w[0]
				pt1=re.sub(r"[^\w]"," ",pt1)
				pt2=re.sub(r"[^\w]"," ",pt2)
				if pt1==pt2: # flexible gazeteer? edit distance-ish?
					at.spottedTriples.append(t[0])
			elif t[2]=="date":
				if pt1==pt2: # flexible gazeteer? edit distance-ish?
					at.spottedTriples.append(t[0])
					at.addSurfaceFeature(token_date)
					break
			elif t[2]=="number":
				if pt1==pt2: # TODO: approximate quantities
					at.spottedTriples.append(t[0])
					at.addSurfaceFeature(token_date)
					break
		res.append(at)

	# fuse together tokens that have been spotted as identical entities and are
	# separated by a comma or similar
	to_delete=[]
	for i,at in enumerate(res[:-3]):
		if res[i+2].hasTriples() and samePredicates(at,res[i+2]) and (res[i+1].text) in [",", ";", "and", "or"]:
			if not res[i+1].text.lower() in ["and","or"]:
				if token_list in at.surface:
					at.surface.remove(token_list)
				at.surface.append(token_tree)
			else:
				at.surface.append(token_list)

			at.text+=" "+res[i+1].text+" "+res[i+2].text

			to_delete.append(res[i+1])
			to_delete.append(res[i+2])

	for d in reversed(to_delete):
		res.remove(d)
	# -- End fuse together

	return res


def createTextSentencePlanFromATlist(tokens):
	"""
	in: tokens = list of alignedToken, comprising one sentence only
	out: a string containing a sentence template, with $$[pred1|pred2]$$ where
		the alignedToken had elements in its .spottedTriples list
	"""
	sentence=""
	for t in tokens:
		if len(t.spottedTriples) > 0:
			rdf_preds=template_bracket_open # $$[
			for i,pred in enumerate(t.spottedTriples):
				rdf_preds+=pred
				if i < len (t.spottedTriples) -1:
					rdf_preds+=template_separator # |
			rdf_preds+=template_bracket_close # ]$$
			sentence+=" "+ rdf_preds
		else:
##			if t.link != phrase_break_token:
			sentence+=" "+t.text

	return sentence

def createSentenceTemplateFromATlist(tokens):
	sp=sentenceTemplate()

	for t in tokens:
		if len(t.spottedTriples) > 0:
			lst=[]
			for i,pred in enumerate(t.spottedTriples):
				lst.append(pred)
			sp.append(lst)
		else:
##			if t.link != phrase_break_token:
			sp.append(t.text)

	return sp

def createNewSentenceTemplateFromATlist(tokens):
	sp=sentenceTemplate()

	for t in tokens:
		if len(t.spottedTriples) > 0:
			t2=templateSlot()
			t2.coref=t.coref
			t2.coref=t.surface[:]
			t2.extend(t.spottedTriples)
			sp.append(t2)
		else:
##			if t.link != phrase_break_token:
			sp.append(t.text)

	return sp
