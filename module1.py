#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      MasterMan
#
# Created:     01/06/2012
# Copyright:   (c) MasterMan 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

phrase_break_token="$$BREAK$$"
class alignedtoken:
    text=""
    link=""
    rdf_triples=[]

def createAlignedTokenList(tokens,text_tokens):
    """ tokens is a list of tuples (pred,word) text is a list of tuples (word,link)
    returns a list of alignedtoken
    """
    res=[]
    for i,w in enumerate(text_tokens):
        at=alignedtoken()
        at.text=w[0]
        at.link=w[1]
        at.rdf_triples=[]


        for t in tokens:
            if w[1].lower()==t[1].lower(): # if the link == the token
                at.rdf_triples.append(t[0])
            else:
                if w[0].lower()==t[1].lower(): # very simple condidion: to-do: feature functions, classifier
                    at.rdf_triples.append(t[0])
        res.append(at)

    return res


def extractSentencesFromATlist(text_tokens,num):
    sentences=[[]]
    count=0
    for token in text_tokens:

        if token.link!=phrase_break_token:
            sentences[-1].append(token)
        else:
            count+=1
            if num > -1:
                if count>=num:
                    return sentences
            sentences.append([])

    return sentences

def selectTemplateWorthySentences(sentences,must_have_list):
    res=[]
    for s in sentences:
        has_alignment=False
        for token in s:
            for pred in token.rdf_triples:
                if pred in must_have_list:
                    has_alignment=True
                    break
            if has_alignment :
                break
        if has_alignment:
            res.append(s)

    return res

def createSentenceTemplateFromATlist(tokens):
    sentence=""
    for t in tokens:
        if len(t.rdf_triples) > 0:
            rdf_preds="$$["
            for i,pred in enumerate(t.rdf_triples):
                rdf_preds+=pred
                if i < len (t.rdf_triples) -1:
                    rdf_preds+="|"
            rdf_preds+=']$$'
            sentence+=" "+ rdf_preds
        else:
##            if t.link != phrase_break_token:
            sentence+=" "+t.text

    return sentence


tokens=[("pred1","word1"),("pred2","word1"),("pred3","word4")]
text=[("WORd1","link1"),("word2","link2"),(".",phrase_break_token),("word3","link3"),("word4","link4")]

##for s in extractSentencesFromATlist(createAlignedTokenList(tokens,text),-1):
##    print createSentenceTemplateFromATlist(s)

##for s in selectTemplateWorthySentences(extractSentencesFromATlist(createAlignedTokenList(tokens,text),-1),["pred3"]):
##    for t in s:
##        print t.text, t.link, t.rdf_triples

##    for t in s:
##        print t.text, t.link, t.rdf_triples
##    print "/SENTENCE"

def countCharsInString(string,separators):
    """
in -> string = string, separators = list of chars
out -> number of separators+1
    """
    chars=1
    for c in string:
        if c in separators:
            chars+=1
    return chars

##res=["a a a a a","a a a a a","a a a a a","a a","a a a a","a a a a","a a a a",]
##res=set(res)
##print sorted(res,key=lambda x:x.count(" "),reverse=True)
def listMultiWordValues(triples):
    """
in -> list of triples
out -> list of unique strings, ordered by number of spaces in them, descending
    """
    res=[]
    for t in triples:
        if " " in t[1]:
            res.append(t[1])

    res=set(res)
    res=sorted(res,key=lambda x:x.count(" "),reverse=True)
    return res


def clusterMultiWordValues(tokens,values):
    """
in -> tokens = list of tuples (word,link), values = list of multi-word strings
out -> list of tokens (tuples (word,link)), where multiple tokens are collapsed
        into single tokens where they match a multi-word value
    """
    pos=0
    res=[]

    if len(values)>0:
        while pos < len(tokens):

            for v in values:
                words=v.split()
                chunk=tokens[pos:pos+len(words)]
                if len(words) == len(chunk):
                    matchesTokens=True
                    for cnt in range(len(words)):
                        if words[cnt].lower() != chunk[cnt][0].lower():
                            matchesTokens=False
                            break
                if matchesTokens:   # we found a match with the current multi-word value
                    text=" ".join([f[0] for f in chunk])
                    link=" ".join([f[1] for f in chunk])
                    res.append((text,link))
                    pos+=len(chunk) # move the pointer by the length of the match in tokens
                    break   # break of the values loop, go to "while"
                pass

            if not matchesTokens:   # if loop over values is over, no match found, add the present token
                res.append(tokens[pos])
                pos+=1
    else:
        res=tokens

    return res

values=['a a a', 'a a']
tokens=[("b","link1"),("A","link2"),("a","link2"),("c","link3"),("A","link4"),("a","link5"),("a","link6"),
("b","link7"),("b","link8"),("b","link9"),("b","link10"),("b","link11"),]
print clusterMultiWordValues(tokens,values)
