﻿#-------------------------------------------------------------------------------
# Name:		RDF
# Purpose:	  everything to do with RDF data: querying DBpedia, storing local
#			   cache of triples, operating with triples in memory
# Author:	  Daniel Duma
#
# Created:	 03/04/2012
# Copyright:   (c) Daniel Duma 2012
# Licence:	 <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python
# coding: utf-8

import urllib, json, os
from os.path import exists
import cPickle
import sqlite3
import codecs
from util import *

globalDBconn=None

class ontoInfo:
	def __init__(self, ontoclass):
		self.ontoclass=ontoclass
		self.predicates=[]

class predInfo:
	def __init__(self, pred):
		self.pred=pred
		self.position=[]

class rdfTriple:
	def __init__(self,subj=u"",pred=u"",value=u"",dataType=u"",lang=u"",reverse=False):
		self.subj=subj
		self.pred=pred
		self.value=value
		self.dataType=dataType
		self.lang=lang
		self.reverse=reverse

	def __repr__(self):
		res="<subj:'" + self.subj + "', pred:'" + self.pred + "', value: '"+self.value+"', dataType:" + self.dataType + ", lang:" + self.lang + ">"
		return codecs.encode(res,"utf-8")

	def asTurtleString(self):
		lang=""
		if self.lang != "":
			lang="@"+self.lang
		return "%s %s \"%s\"%s"%(self.subj,self.pred,self.value,lang)

	def asGraphPath(self):
		return "%s%s%s"%(self.subj,gp_sep,self.pred)

cache_dir="c:\\NLP\\thesis\\cache\\"
cache_dbpath = cache_dir+ "cache.db"

##default_sparql_endpoint="http://dbpedia.org/sparql/"
##default_sparql_endpoint="http://factforge.net/sparql/"
##default_sparql_endpoint="http://live.dbpedia.org/sparql"
default_sparql_endpoint="http://dbpedia-live.openlinksw.com/sparql"
extra_sparql_endpoint="http://ff-dev.ontotext.com/sparql"
factforge_endpoint="http://factforge.net/sparql/"

cached_articles=set()

useless_predicates=["http://www.w3.org/2000/01/rdf-schema#comment","http://dbpedia.org/ontology/abstract",
"http://dbpedia.org/property/filename","http://dbpedia.org/ontology/wikiPageExternalLink",
"http://dbpedia.org/property/float", "http://purl.org/dc/terms/subject", "http://www.w3.org/2002/07/owl#sameAs",
"http://dbpedia.org/ontology/wikiPageDisambiguates","http://dbpedia.org/ontology/wikiPageRedirects",
"http://dbpedia.org/property/birthPlace","http://dbpedia.org/property/deathPlace","http://dbpedia.org/property/align",
"http://dbpedia.org/ontology/thumbnail","http://dbpedia.org/property/description",
"http://dbpedia.org/property/wikiPageUsesTemplate","http://dbpedia.org/property/derivedfrom"]



classes_too_basic=["http://www.w3.org/2002/07/owl#Thing","http://schema.org/Organization",
"http://umbel.org/umbel/rc/Organization", "http://dbpedia.org/ontology/Agent",
"http://dbpedia.org/ontology/Organisation", "http://dbpedia.org/ontology/Place",
"http://schema.org/Place", "http://dbpedia.org/ontology/Person", "http://schema.org/Person",
"http://dbpedia.org/ontology/Work"]





useful_languages=["en","","uri"]

# HTTP URL is constructed accordingly with JSON query results format in mind.
def sparqlQuery(query, baseURL, format="application/json"):
	params={
	"default-graph": "",
##	"should-sponge": "soft",
	"query":  query,
	#"debug": "on",
	#"timeout": "",
	"format": format,
	#"save": "display",
	"accept":format
	#"fname": ""
	}
	try:
		querypart=urllib.urlencode(params)
	except:
		return "Unicode Error"

	response = urllib.urlopen(baseURL,querypart).read()
	res=response
	try:
		res=json.loads(response)
	except:
		pass
	return res


def getTriplesFromSparql(wiki_link,override_subj=None):
	"""
	in-> wiki_link = string
	out -> list of rdfTriple
	"""
	res=[]

##	query="""
##PREFIX foaf: <http://xmlns.com/foaf/0.1/>
##
##SELECT ?dbpedia_link ?attr ?val
## WHERE {
## ?dbpedia_link foaf:page <http://en.wikipedia.org/wiki/%s> .
## ?dbpedia_link ?attr ?val
##}
## """ % wiki_link
	query="""
SELECT ?pred ?obj
 WHERE {
 <http://dbpedia.org/resource/%s> ?pred ?obj.
}
 """ % wiki_link
	data=""
	while data=="":
		data=sparqlQuery(query, default_sparql_endpoint)

	if isinstance(data,str):
		failcount=1
		print "SPARQL error!\n Result: ",data
		while isinstance(data,str) and failcount < 3:
			print "Trying again..."
			failcount+=1
			data=sparqlQuery(query, "http://dbpedia.org/sparql/")
		raise Exception

	for d in data.items()[1][1]['bindings']:
##		print d.items()[1][1]["value"], " = ", d.items()[2][1]["value"]
		pred=d.items()[0][1]
		obj=d.items()[1][1]
		if obj["type"]=="literal":
			if "xml:lang" in obj:
				lng=obj["xml:lang"]
			else: lng=""
		else:
			lng=""

		new_triple=rdfTriple()
##		if override_subj is not None:
##			new_triple.subj=override_subj
##		else:
		new_triple.subj=fullTripleSubject(wiki_link)
		new_triple.pred=pred["value"]
		new_triple.value=obj["value"]
		if obj["type"]=="typed-literal":
			new_triple.dataType=obj["datatype"]
		else:
			new_triple.dataType="literal"

##		print obj
		new_triple.lang=lng
		if new_triple.pred not in useless_predicates and (new_triple.lang in useful_languages):
			res.append(new_triple)

##	print json.dumps(data)
	# save space in db by removing abstracts, comments and other languages
##	res=filterOutUselessTriples(res)

	cached_articles.add(wiki_link)
	pickleTriplesToDB(res,wiki_link)
	res=overrideTriplesSubject(res,override_subj)
	return res

def getReverseTriplesFromSparql(wiki_link,override_value=None):
	"""
	in-> wiki_link = string
	out -> list of rdfTriple
	"""
	res=[]

	query="""
PREFIX foaf: <http://xmlns.com/foaf/0.1/>

SELECT ?subj, ?pred
 WHERE {
 ?subj ?pred <http://dbpedia.org/resource/%s> .
}
 """ % wiki_link

	data=""
	while data=="":
		data=sparqlQuery(query, "http://dbpedia.org/sparql/")

	if isinstance(data,str):
		failcount=1
		print "SPARQL error!\n Result: ",data
		while isinstance(data,str) and failcount < 3:
			print "Trying again..."
			failcount+=1
			data=sparqlQuery(query, "http://dbpedia.org/sparql/")
		raise Exception

	for d in data.items()[1][1]['bindings']:
		pred=d.items()[0][1]
		subj=d.items()[1][1]

		new_triple=rdfTriple()
		new_triple.reverse=True
		new_triple.subj=fullTripleSubject(wiki_link)
		new_triple.pred=pred["value"]
		new_triple.value=subj["value"]
		new_triple.dataType="uri"
		new_triple.lang=""

##		print obj
		if new_triple.pred not in useless_predicates and (new_triple.lang in useful_languages):
			res.append(new_triple)

##	print json.dumps(data)
	# save space in db by removing abstracts, comments and other languages
##	res=filterOutUselessTriples(res)

	cached_articles.add(wiki_link)
	pickleTriplesToDB(res,wiki_link)
	res=overrideTriplesSubject(res,override_subj)
	return res


def quickTripleQuery(subj,pred,override_subj=None):
	"""
	in -> subj = subj URI, pred = pred URI, override_subj = string to set subj to in returned triples
	out -> list of rdfTriple
	"""
	subj=expandStringNamespaces(subj)
	pred=expandStringNamespaces(pred)

	if tripleIsNonExistant(subj,pred):
		return []

	res=loadTripleFromDB(subj,pred)
##	print "returned from DB:",len(res)
	if len(res)==0:
##		print "
		res=sparqlTripleQuery(subj,pred)
		if len(res)==0:
			saveNonExistantTripleToDB((subj,pred))
		else:
			for triple in res:
				saveTripleToDB(triple)

			res=overrideTriplesSubject(res,override_subj)
	else:
		res=overrideTriplesSubject(res,override_subj)
	return res

def sparqlTripleQuery(subj,pred,override_subj=None):
	res=[]
	query="""
SELECT ?obj
 WHERE {
 <%s> <%s> ?obj .
 #FILTER (langMatches(lang(?obj),"") || langMatches(lang(?obj),"en") )
}
 """ % (subj,pred)

	data=""
	while data=="":
		data=sparqlQuery(query, default_sparql_endpoint)

	if data=="Unicode Error": return []

	if isinstance(data,str):
		print "Result: ",data
		raise Exception


	for d in data.items()[1][1]['bindings']:
		obj=d.items()[0][1]
		if obj["type"]=="literal":
			if "xml:lang" in obj:
				lng=obj["xml:lang"]
			else:
				lng=""
		else:
			if "xml:lang" in obj:
				lng=obj["xml:lang"]
			else:
				lng=""

##		obj["datatype"] !!
		new_triple=rdfTriple()
		if override_subj is not None:
			new_triple.subj=override_subj
		else:
			new_triple.subj=subj
		new_triple.pred=pred
		new_triple.value=obj["value"]
		new_triple.dataType=obj["type"]
		new_triple.lang=lng
		if new_triple.pred not in useless_predicates and (new_triple.lang in useful_languages):
			res.append(new_triple)
	return res

def tupletoRdfTriple(tup):
	k=rdfTriple()
	k.subj=tup[0]
	k.pred=tup[1]
	k.value=tup[3]
	k.dataType=tup[4]
	k.lang=tup[5]
	return k

def extractPredFromGP(gp):
	return gp.replace(self_pointer+gp_sep,"")

def filterOutUselessTriples(triples):
	triples=selectTriplesLanguage(triples,"en")
	res=[]
	for r in triples:
		if r.pred not in useless_predicates and not r.pred.startswith("http://dbpedia.org/resource/Template:"):
			res.append(r)
	return res

def overrideTriplesSubject(triples,override_subj):
	if override_subj is None:
		return triples
	res=[]
	for t in triples:
		if isinstance(t,rdfTriple):
			t.subj=override_subj
			res.append(t)
		else:
			raise Exception
			res.append((override_subj,t[1],t[2],t[3],t[4]))
	return res

def getTriplesUsingCache(link,override_subj=None):
	if link in cached_articles:
		return overrideTriplesSubject(unPickleTriplesDB(link),override_subj)
	else:
		print "Triples for %s not in cache, querying DBpedia..." % link
		return getTriplesFromSparql(link,override_subj)
	pass

def selectTriplesLanguage(triples,lang):
	res=[]
	for t in triples:
##		t=rdfTriple(t)
		if (t.lang == "") or t.lang == "uri":
			res.append(t)
		else:
			if t.lang.lower() == lang.lower():
				res.append(t)
	return res

def generateLabelFromURI(uri):
	return space_out_camel_case(extractClassName(uri)).replace("_"," ").replace("  "," ")

def getLabelFor(uri):
	"""
	Returns the rdfs:label of a URI, filters for languages (en and null)
	If label is not found, it creates it by de-camel-casing the URI
	"""
	labels=quickTripleQuery(uri,rdfs_label)
	for l in labels:
##			print l
		if l.dataType != "uri" and (l.lang == "" or l.lang.lower() == "en") :
			return l.value

	res=generateLabelFromURI(uri)
	return res

def getFoafNameOrLabelFor(uri):
	"""
	Returns the foaf:name or rdfs:label of a URI (first found), filters for languages (en and null)
	If label is not found, it creates it by de-camel-casing the URI
	"""
	names=quickTripleQuery(uri,foaf_name)
	for l in names:
		if l.dataType != "uri" and (l.lang == "" or l.lang.lower() == "en") :
			return l.value

	labels=quickTripleQuery(uri,rdfs_label)
	for l in labels:
		if l.dataType != "uri" and (l.lang == "" or l.lang.lower() == "en") :
			return l.value

	res=space_out_camel_case(extractClassName(uri))
	return res


def fullTripleSubject(wiki_link):
	"""
	Return dbpedia: URI for wiki_link
	"""
	if not wiki_link.startswith("http:"):
		res="http://dbpedia.org/resource/"+wiki_link
	else:
		res=wiki_link
	return res

def shortenTripleNamespaces(triples):
	for t in triples:
		for pf in prefix_long:
			if t.pred.find(pf) <> -1:
				t.pred=t.pred.replace(pf,prefix_long[pf])
			if t.subj.find(pf) <> -1:
				t.subj=t.subj.replace(pf,prefix_long[pf])
	return triples

def expandTripleNamespaces(triples):
	for t in triples:
		for pf in prefix_short:
			if t.pred.find(pf) <> -1:
				t.pred=t.pred.replace(pf,prefix_short[pf])
			if t.subj.find(pf) <> -1:
				t.subj=t.subj.replace(pf,prefix_short[pf])
	return triples

def shortenStringNamespaces(s):
	for pf in prefix_long:
		if s.find(pf) <> -1:
			s=s.replace(pf,prefix_long[pf])
	return s

def expandStringNamespaces(s):
	for pf in prefix_short:
		if s.find(pf) <> -1:
			s=s.replace(pf,prefix_short[pf])
	return s


def pickleTriplesToDB(triples,wiki_link):
	if globalDBconn is not None:
		c=globalDBconn.cursor()

##		dumpstr=cPickle.dumps(triples,protocol=0)
		dumpstr="<see individual triples>"
##		text_factory=str
		c.execute(u"insert into cached_articles (wiki_link,triples) values (?,?)",(wiki_link,dumpstr))
		globalDBconn.commit()
		c.close()
		for t in triples:
			saveTripleToDB(t)


def unPickleTriplesDB(wiki_link):
##	res=[]
##	if globalDBconn is not None:
##		c=globalDBconn.cursor()
##		c.execute("select triples from cached_articles where wiki_link=?",(wiki_link,))
##		rows=c.fetchall()
##		res=cPickle.loads(rows[0][0])
##	else:
##		raise Exception

##	return res

	if globalDBconn is not None:
		c=globalDBconn.cursor()
		c.execute(u"select subj,pred,obj,type,lang from all_triples where lang in ('en','','uri') and subj=?",(fullTripleSubject(wiki_link),))
		rows=c.fetchall()
		c.close()
		res=[]
		for r in rows:
			new_triple=rdfTriple(r[0],r[1],r[2],r[3],r[4])
			res.append(new_triple)
		return res
	else:
		raise Exception


def listArticleTriplesByPred(pred,triples):
	"""
	in -> pred = string literal of RDF predicate, triples = list of rdfTriples
	out -> list of value(s)
	"""
	res=[]

	for i,t in enumerate(triples):
		if t.pred==pred:
			res.append(t.value)
	return res

def saveTripleToDB(triple):
	if globalDBconn is not None:
		c=globalDBconn.cursor()
		c.execute(u"insert into all_triples (subj,pred,obj,type,lang) values (?,?,?,?,?)",
		(triple.subj,triple.pred,triple.value,triple.dataType,triple.lang))
		globalDBconn.commit()
		c.close()

def saveNonExistantTripleToDB(triple):
	if globalDBconn is not None:
		c=globalDBconn.cursor()
		c.execute(u"insert into non_existant_triples (subj,pred) values (?,?)",(triple[0],triple[1]))
		globalDBconn.commit()
		c.close()

def loadTripleFromDB(subj,pred):
	if globalDBconn is not None:
		c=globalDBconn.cursor()
		c.execute(u"select subj,pred,obj,type,lang from all_triples where lang in ('en','','uri') and subj=? and pred =?",(subj,pred))
		rows=c.fetchall()
		c.close()
		res=[]
		for r in rows:
			new_triple=rdfTriple(r[0],r[1],r[2],r[3],r[4])
			res.append(new_triple)
		return res
	else:
		raise Exception

def tripleIsNonExistant(subj,pred):
	if globalDBconn is not None:
		c=globalDBconn.cursor()
		c.execute(u"select subj,pred from non_existant_triples where subj=? and pred =?",(subj,pred))
		rows=c.fetchall()
		c.close()
		if len(rows)==0:
			return False
		else:
			return True
	else:
		raise Exception

def loadCache():
	global globalDBconn
	if exists(cache_dbpath):
		globalDBconn=sqlite3.connect(cache_dbpath)
		globalDBconn.text_factory=str
		c=globalDBconn.cursor()
		c.execute("select wiki_link from cached_articles")
		rows=c.fetchall()
		for row in rows:
			cached_articles.add(row[0])
		c.close()

def createCacheDB():
	if not exists(cache_dbpath):
		conn=sqlite3.connect(cache_dbpath)
		c=conn.cursor()
		c.execute("create table all_triples (id int primary key, subj text, pred text, obj text, type text, lang text)")
		c.execute("create table cached_articles (wiki_link text, triples text, date numeric)")
		c.execute("create table non_existant_triples (subj text, pred text)")
		conn.commit()
		c.close()
		conn.close()

def getBasicSelfRE(wiki_link):
	"""
	Return a full Referring Expression based on the rdfs:label of the entity
	"""
	self_labels=quickTripleQuery(fullTripleSubject(wiki_link),self_REG_default)
	if len(self_labels) > 0:
		self_basic_RE=self_labels[0].value
	else:
		self_basic_RE=generateSLfromLink(wiki_link)
	return self_basic_RE

createCacheDB()
loadCache()

##getReverseTriplesFromSparql("Johann_Sebastian_Bach","$self")

##print "rdfm is not supposed to be run! Run main.py instead"