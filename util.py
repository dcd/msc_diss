#-------------------------------------------------------------------------------
# Name:		module1
# Purpose:
#
# Author:	  Daniel Duma
#
# Created:	 05/04/2012
# Copyright:   (c) Daniel Duma 2012
#-------------------------------------------------------------------------------
#!/usr/bin/env python

import unicodedata
import inflect
import re
import urllib

from collections import defaultdict
import codecs

inf_engine=inflect.engine()

phrase_break_token="$$BREAK$$"
template_bracket_open=u"@["
template_bracket_close=u"]@"
template_separator=","
self_pointer="$self"
token_gen="$gen"
token_nom="$nom"
token_list="$list"
token_tree="$tree" # subordinate list
token_ordinal="$ord"
token_date="$date"
gp_sep="\\"
gp_surface_sep=" "
gp_surface_marker="$!"

small_value_threshold=32
image_file_extensions=[".jpg",".png",".tga",".svg",".bmp"]

self_genitive=self_pointer+gp_sep+token_gen
self_nominative=self_pointer+gp_sep+token_nom

rdfs_label="http://www.w3.org/2000/01/rdf-schema#label"
foaf_name="http://xmlns.com/foaf/0.1/name"
rdf_type="http://www.w3.org/1999/02/22-rdf-syntax-ns#type"
short_rdf_type="rdf:type"
dbp_res="http://dbpedia.org/resource/"
dbp_ont="http://dbpedia.org/ontology/"
xsd_date="http://www.w3.org/2001/XMLSchema#date"
owl_sameas="http://www.w3.org/2002/07/owl#sameAs"
owl_equivalent="http://www.w3.org/2002/07/owl#equivalentProperty"

prefix_long={"http://dbpedia.org/resource/":"dbpedia:",
"http://dbpedia.org/ontology/":"dbont:",
"http://dbpedia.org/property/":"dbprop:",
"http://xmlns.com/foaf/0.1/":"foaf:",
"http://www.w3.org/2000/01/rdf-schema#":"rdfs:",
"http://www.w3.org/1999/02/22-rdf-syntax-ns#":"rdf:",
"http://purl.org/dc/elements/1.1/":"purl:"}

prefix_short={}
for p in prefix_long:
	prefix_short[prefix_long[p]]=p


pronouns="he she it they".split()
posessives={"he":["his"],"she":["her","hers"],"it":["its"],"they":["their","theirs"]}


self_REG_default=rdfs_label

self_corefering_predicates=["http://www.w3.org/2000/01/rdf-schema#label","http://dbpedia.org/property/name",
"http://xmlns.com/foaf/0.1/name","http://xmlns.com/foaf/0.1/surname", 'http://xmlns.com/foaf/0.1/givenName',
'http://xmlns.com/foaf/0.1/surname', 'http://dbpedia.org/property/idx',"http://dbpedia.org/property/birthName",
"http://dbpedia.org/ontology/birthName"]

word_window_size=15

max_rank=15
maxNgram=4

class classModel:
	def __init__(self,name):
		self.name=name
		self.pronouns=defaultdict(int)
		self.sentences=[]
		self.content_model={}
		self.poolngrams={}
		self.ngrams={}
		self.collected_spottings=[]
		self.equivalent=defaultdict(list)
		self.co_oc=defaultdict(dict)
		#self.co_oc_count=defaultdict(int)
		self.co_oc_counts=[]
		self.pools=[]
		self.article_count=0
		self.articles_trained=[]
		self.dice_coefs=defaultdict(int)

		for i in range(maxNgram):
			self.ngrams[i+1]=defaultdict(int)
			self.poolngrams[i+1]=defaultdict(int)

	def bestPronoun(self):
		return sorted(self.pronouns.iteritems(),key=lambda x:x[1],reverse=True)[0][0]
	def __repr__(self):
		return "<classModel name='"+self.name+"'>"

class sentenceTemplate(list):
	def __init__(self):
		self.cnt=0
		self.rank=defaultdict(int)

	def toStringTemplate(self):
		res=u""
		cnt=1
		for t in self:
			if isinstance(t,basestring):
				res+=t+" "
			if isinstance(t,templateSlot):
				res+=template_bracket_open+u"x"+unicode(cnt)+template_bracket_close+u" "
				cnt+=1
		return res

	def __repr__(self):
		res=u"<"
		for t in self:
			if isinstance(t,str):
				res+=t+u" "
			if isinstance(t,unicode):
				try:
					res+=codecs.encode(t,"utf-8")+" "
##				res+=unicode(t,errors="replace")+" "
				except:
					res+="<unicode>"
			if isinstance(t,templateSlot):
##				res+=str(t)
				res+=template_bracket_open
				for i,triple in enumerate(t):
					triple=str(triple)
					res+=triple
					if i < len(t) - 1:
						res+=template_separator
				res+=template_bracket_close+u" "
		res+=">"
		return str(res)

class alignedToken:
	def __init__(self,text="",link=""):
		self.text=text
		self.link=link
		self.coref=""
		self.spottedTriples=[]
		self.surface=[]
	def __repr__(self):
		try:
			res=str("<text:'%s', link:'%s', coref:'%s', surface:'%s', spottedTriples:%s>" % (self.text, self.link,self.coref,self.surface,str(self.spottedTriples)))
		except:
			res="<unicode error>"
		return res
	def hasTriples(self):
		return (len(self.spottedTriples)>0)
	def copyFrom(self,other):
		self.text=other.text
		self.link=other.link
		self.coref=other.coref
		self.spottedTriples=other.spottedTriples[:]
		self.surface=other.surface[:]
	def addSurfaceFeature(self,f):
		if f not in self.surface: self.surface.append(f)

class templateSlot(list):
	def __init__(self):
		self.coref=""
		self.surface=[]

class predPool(list):
	def __init__(self, name=""):
		self.count_per_article=0
		self.issued=0
		self.values=[]
		self.surface=[]
		self.name=name
	def getMain(self):
		res=""
		if len(self) > 0: res=self[0]
		return res

class listWithSurface(list):
	def __init__(self):
		self.surface=[]

class rdfGraphPath(list):
	def __init__(self,items):
		self.extend(items)
	def __repr__(self):
		res=u""
		for item in self:
			res+=item+gp_sep
		res=res.strip("\\")
		return res
	def asTurtle(self):
		if len(self) > 3: bits=self[-3:]
		else: bits=self[:]
		return "%s %s \"%s\""%(bits[0],bits[1],bits[2])

# [$main\dbpedia:leaderName\dc:firstName]

def normalizeCharacters(s):
	return ''.join(c for c in unicodedata.normalize('NFKD', s)
				   if unicodedata.category(c) != 'Mn')

def normalizeDatesWithRegEx(text):
	replist=[]
	for x in rxdate3.finditer(text):
		sd=" %04s-%02d-%02d "%(int(x.group(3)),repldict[x.group(2).capitalize()],int(x.group(1)))
##		print sd
		#d=datetime.date(int(x.group(6)), repldict[x.group(1)], int(x.group(3)))
		replist.append((x.group(0),sd))

	for x in rxdate4.finditer(text):
		sd=" %04s-%02d-%02d "%(int(x.group(3)),repldict[x.group(1).capitalize()],int(x.group(2)))
##		print sd
		#d=datetime.date(int(x.group(6)), repldict[x.group(1)], int(x.group(3)))
		replist.append((x.group(0),sd))

	for g,d in replist:
		text=text.replace(g,d)

	replist=[]

	for x in rxdate1.finditer(text):
##		d=datetime.date(int(x.group(6)), repldict[x.group(3)], int(x.group(1)))
		sd=" %04s-%02d-%02d "%(int(x.group(6)),repldict[x.group(3).capitalize()],int(x.group(1)))
##		print sd
		replist.append((x.group(0),sd))

	for x in rxdate2.finditer(text):
		sd=" %04s-%02d-%02d "%(int(x.group(6)),repldict[x.group(1).capitalize()],int(x.group(3)))
##		print sd
		#d=datetime.date(int(x.group(6)), repldict[x.group(1)], int(x.group(3)))
		replist.append((x.group(0),sd))

	for g,d in replist:
##		text=text.replace(g,"$date(%s)"%d)
		text=text.replace(g,d)
##		print "FUCK YEAH!!"

	return text

numbers_lexical={"one":1,"two":2,"thee":3,"four":4,"five":5,"six":6,"seven":7,
"eight":8,"nine":9,"ten":10,"eleven":11,"twelve":12,"thirteen":13,"fourteen":14,
"fifteen":15,"sixteen":16,"seventeen":17,"eighteen":18,"nineteen":19,"twenty":20,
"thirty":30,"fourty":40,"fifty":50}

def normalizeNumbers(text):
	for nl in numbers_lexical:
		text=re.sub(r"([^\w])"+nl+"([^\w])",numbers_lexical[nl],text)
	return text
	pass

def removeStopWords(s):
	res=[]
##	words=s.split()
	for w in s:
		if w.lower() not in english_stopwords:
			res.append(w)
	return res

def tagPhraseBreaks(text_tokens):
	open_par=0
	tokens_since_last_par=0
	for cnt in range(len(text_tokens)):
		tokens_since_last_par+=1
		if tokens_since_last_par > 15:
			open_par-=1
			tokens_since_last_par=0
		if text_tokens[cnt][0] in ["(","["]:
			open_par+=1
			tokens_since_last_par=0
		if text_tokens[cnt][0] in [")","]"]:
			open_par-=1
			tokens_since_last_par=0
		if open_par <= 0:
			if text_tokens[cnt][0] in ["."]:
				tp=text_tokens[cnt]
				text_tokens[cnt] = (tp[0],phrase_break_token)
##			if text_tokens[cnt][0].find("==") != -1:
##				if len(text_tokens) > cnt+1 and text_tokens[cnt+1].find("==") == -1:


	return text_tokens

def slotsWithInfo(sentence):
	total=0
	for token in sentence:
		if len(token.spottedTriples) > 0:
			if token.coref != self_pointer:
				total+=1
	return total

def findPathInPools(model,gp):
	"""
	Returns the pool in a model that contains gp
	"""
	for p in model.pools:
		if gp in p:
			return p
	return None


def predicateInTemplate(pred,sent):
	for token in sent:
		if isinstance(token, templateSlot):
			for gp in token:
				if gp==pred:
					return True
	return False

def getOrphanPools(model):
	"""
	Returns a list of predicate pools in a model that would not be expressed using the
	sentence templates in the model.

	Only counts pools with loaded values.
	"""
	res=[]
	for pool in model.pools:
		if len(pool.values) == 0:
			continue
		found=False
		for pred in pool:
			if found: break
			for s in model.sentences:
				if predicateInTemplate(pred,s):
					found=True
					break

		if not found:
			res.append(pool)
	return res


def isHexNumber(s):
	if s.islower():
		return False

	try:
		num=int(s,16)
	except:
		return False
	return True


def isNumberAndIsLessThan(val, thold):
	try:
		num=int(val)
		if num < thold:
			return True
	except:
		pass
	return False


def buildGraphPath(items):
	res=""
	for cnt,i in enumerate(items):
		res+=i
		if cnt < len(items)-1:
			res+=gp_sep
	return res

def wikifyTitle(title):
	"""
in -> plain string with title
out -> "wikified" title
	"""
	title=title.replace(" ","_")

	try:
##		title=title.encode('iso-8859-1')
##		title=title.decode('utf-8')
		title2=urllib.quote(title)
	except:
		title2=title
	return title2

def listToString(l,spacer=" "):
	return spacer.join(l).strip(spacer)

def joinLists(l):
	res=[]
	for item in l:
		for subitem in item:
			res.append(subitem)
	return res

numToMonth={1:"January",2:"February",3:"March",4:"April",5:"May",6:"June",
7:"July",8:"August",9:"September",10:"October",11:"November",12:"December"}

def surfaceDate(date):
	"""
	Return a nice-looking date from a standard YYYY-MM-DD format
	"""
	yy=int(date[:4])
	mm=int(date[5:7])
	dd=int(date[8:10])

	return str(dd)+" "+numToMonth[mm]+" "+str(yy)

def makePluralSingular(word):
	try:
		res=inf_engine.singular_noun(word)
		if res==False:
			res=word
	except:
		res=word
	return res

def makeSingularPlural(word):
	try:
		res=inf_engine.plural_noun(word)
		if res==False:
			res=word
	except:
		res=word
	return res

def space_out_camel_case(s):
	"""Adds spaces to a camel case string.  Failure to space out string returns the original string.
	Example:
	>>> space_out_camel_case('DMLSServicesOtherBSTextLLC')
	'DMLS Services Other BS Text LLC'
	"""

	return re.sub('((?=[A-Z][a-z])|(?<=[a-z])(?=[A-Z]))', ' ', s).strip()

def is_number(s):
	try:
		float(s)
		return True
	except ValueError:
		return False

def is_URI(s):
	pos=s.find("http://")
	if (pos > -1) and (pos < 2):
		return True
	else:
		return False

def is_rdf_date(s):
	if re.match(r"(1|2)[0-9][0-9]{2}[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])",s,re.DOTALL):
		return True
	else:
		return False

def generateSLfromLink(link):
	res=[]
	label=space_out_camel_case(link).replace("_"," ").replace("  "," ")
	res.append(label)
	return res

def extractClassName(uri):
	""" Given an RDF URI, it returns the class name
	Example:
		http://dbpedia.org/resource/Beethoven -> Beethoven
		http://dbpedia.org/resource/bla#something -> something
	"""
	res=""
	pos=uri.rfind("#")
	if pos > -1:
		res=uri[pos+1:]
	else:
		pos=uri.rfind("/")
		if pos > -1:
			res=uri[pos+1:]
		else:
			res=uri
	return res

def findSentence(sentences,sent):
	for i,s in enumerate(sentences):
		if s.toStringTemplate() == sent.toStringTemplate():
			return i
	return -1

def iterateBuildNgrams(ngramlist,pos,result=[]):
	res=[]
	if pos == len(ngramlist): return []

	for val in ngramlist[pos]:
		tmp=result[:]
		tmp.append(val)
		if pos < len(ngramlist) -1:
			res.extend(iterateBuildNgrams(ngramlist,pos+1,tmp))
		else:
			res.append(tmp)

	return res


##print extractSentencesFromTokens([("a",""),("b",""),(".",phrase_break_token),("a",""),("b",""),(".",phrase_break_token)],2)
##print makePluralSingular("landlocked countries")

##	regExDate=r"(?i)((?:(?:[0-2]?\d{1})|(?:[3][01]{1})))(?![\d])(\s+)((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))(|,)(\s+)((?:(?:[1]{1}\d{1}\d{1}\d{1})|(?:[2]{1}\d{3})))(?![\d])|(((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))(\s+)((?:(?:[0-2]?\d{1})|(?:[3][01]{1})))(?![\d])(|,)(\s+)((?:(?:[1]{1}\d{1}\d{1}\d{1})|(?:[2]{1}\d{3})))(?![\d]))"
regExDate1=r"((?:(?:[0-2]?\d{1})|(?:[3][01]{1})))(?![\d])(\s+)((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))(|,)(\s+)((?:(?:[1]{1}\d{1}\d{1}\d{1})|(?:[2]{1}\d{3})))(?![\d])"
regExDate2=r"((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))(\s+)((?:(?:[0-2]?\d{1})|(?:[3][01]{1})))(?![\d])(|,)(\s+)((?:(?:[1]{1}\d{1}\d{1}\d{1})|(?:[2]{1}\d{3})))(?![\d])"
# and now with [[ ]]:
regExDate3=r"\[{2}((?:(?:[0-2]?\d{1})|(?:[3][01]{1})))(?![\d])\s+((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))\]{0,2}[,]?\s+\[{0,2}((?:(?:[1]{1}\d{1}\d{1}\d{1})|(?:[2]{1}\d{3})))\]{2}(?![\d])"
regExDate4=r"\[{2}((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))\s+((?:(?:[0-2]?\d{1})|(?:[3][01]{1})))(?![\d])\]{0,2}[,]?\s+\[{0,2}((?:(?:[1]{1}\d{1}\d{1}\d{1})|(?:[2]{1}\d{3})))\]{2}(?![\d])"

regExNum1=r"(\d+)\s+(hundred|thousand|million|billion)\s*(?:thousand|million|billion|trillion)?\s*(?:million|billion|trillion)?\s*"


rxdate1=re.compile(regExDate1,re.IGNORECASE|re.DOTALL)
rxdate2=re.compile(regExDate2,re.IGNORECASE|re.DOTALL)
rxdate3=re.compile(regExDate3,re.IGNORECASE|re.DOTALL)
rxdate4=re.compile(regExDate4,re.IGNORECASE|re.DOTALL)


repldict={"January":1,"Jan":1,"February":2,"Feb":2,"March":3,"Mar":3,"April":4,"Apr":4,"May":5,"June":6,
"Jun":6,"July":7,"Jul":7,"August":8,"Aug":8,"September":9,"Sept":9,"October":10,"Oct":10,"November":11,"Nov":11,"December":12,"Dec":12}

english_stopwords = """a an about after again against all also and another any around as ask
 at back because become before begin between both but by can
 come consider could develop do during each early even
 feel few find first follow for form from get give go good great
 have he here high hold how however I if in
 into it just keep know large last late lead leave life like line little
 long many may mean might more most move much must never
new no not now of off old on one only open or other out over own
 real right run same say school see seem set she should show since small so some stand
state still such system take tell than that the then there these they think
this those through time to too turn under up use very want way we well what when
where which while who with without would write year you
is are were be was has 's i you my your his her me us been""".split()
