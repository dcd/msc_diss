#-------------------------------------------------------------------------------
# Name:        data_analysis
# Purpose:
#
# Author:      Daniel Duma
#
# Created:     08/06/2012
# Copyright:   (c) Daniel Duma 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from wiki import *
import re
import codecs


##    regExDate=r"(?i)((?:(?:[0-2]?\d{1})|(?:[3][01]{1})))(?![\d])(\s+)((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))(|,)(\s+)((?:(?:[1]{1}\d{1}\d{1}\d{1})|(?:[2]{1}\d{3})))(?![\d])|(((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))(\s+)((?:(?:[0-2]?\d{1})|(?:[3][01]{1})))(?![\d])(|,)(\s+)((?:(?:[1]{1}\d{1}\d{1}\d{1})|(?:[2]{1}\d{3})))(?![\d]))"
regExDate1=r"((?:(?:[0-2]?\d{1})|(?:[3][01]{1})))(?![\d])(\s+)((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))(|,)(\s+)((?:(?:[1]{1}\d{1}\d{1}\d{1})|(?:[2]{1}\d{3})))(?![\d])"
regExDate2=r"((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))(\s+)((?:(?:[0-2]?\d{1})|(?:[3][01]{1})))(?![\d])(|,)(\s+)((?:(?:[1]{1}\d{1}\d{1}\d{1})|(?:[2]{1}\d{3})))(?![\d])"
# and now with [[ ]]:
regExDate3=r"((\[{2})(?:(?:[0-2]?\d{1})|(?:[3][01]{1})))(?![\d])(\s+)((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))(|\]{2})(|,)(\s+)(|\[{2})((?:(?:[1]{1}\d{1}\d{1}\d{1})|(?:[2]{1}\d{3})))(\]{2})(?![\d])"
regExDate4=r"((\[{2})(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))(\s+)((?:(?:[0-2]?\d{1})|(?:[3][01]{1})))(?![\d])(|\]{2})(|,)(\s+)(|\[{2})((?:(?:[1]{1}\d{1}\d{1}\d{1})|(?:[2]{1}\d{3})))(\]{2})(?![\d])"

repldict={"January":1,"Jan":1,"February":2,"Feb":2,"March":3,"Mar":3,"April":4,"Apr":4,"May":5,"June":6,
"Jun":6,"July":7,"Jul":7,"August":8,"Aug":8,"September":9,"Sept":9,"October":10,"Oct":10,"November":11,"Nov":11,"December":12,"Dec":12}

date1=re.compile(regExDate1,re.IGNORECASE|re.DOTALL)
date2=re.compile(regExDate2,re.IGNORECASE|re.DOTALL)
date3=re.compile(regExDate3,re.IGNORECASE|re.DOTALL)
date4=re.compile(regExDate4,re.IGNORECASE|re.DOTALL)

regExNum1=r"(\d+)\s+(hundred|thousand|million|billion)\s*(?:thousand|million|billion|trillion)?\s*(?:million|billion|trillion)?\s*"

numbers1=re.compile(regExNum1,re.IGNORECASE|re.DOTALL)

window_size=25
printed=False

def logToFile(string):
    print string
    fout.write(string+"\n")

def match_article(wl):
    text=loadArticleFromDB(wl)
    text=getSurfaceTextFromTextTokens(tokenizeWikiMarkup(text))

    if len(text) == 0: return False
    printed=False

    for x in date1.finditer(text):
        if not printed:logToFile("In "+wl+":")
        printed=True
        logToFile( text[x.start()-window_size:x.end()+window_size])

    for x in date2.finditer(text):
        if not printed:logToFile("In "+wl+":")
        printed=True
        logToFile( text[x.start()-window_size:x.end()+window_size])

    for x in numbers1.finditer(text):
        if not printed:logToFile("In "+wl+":")
        printed=True
        logToFile(text[x.start()-window_size:x.end()+window_size])

    return True

def walkThroughArticles():
    for wl in sorted(wiki_articles):
        match_article(wl)

def processArticleListFromFile(filename, num=0):
    successful=[]
##    from time import sleep
    f=codecs.open(filename,"r",encoding="utf-8")

    cnt=0

    for line in f :
##        sleep(1)
        cnt+=1
        if num > 0 and cnt >= num:
            break

        line=line.strip("\n\r ")
        line=wikifyTitle(line)
##        print line

        if match_article(line):
            successful.append(line)

    f.close()
##    f2=codecs.open("successful.txt","w",encoding="utf-8")
##    for s in successful:
##        f2.write(s+"\n")
##    f2.close()

##fout=codecs.open("data_output_countries.txt","w",encoding="utf-8")
##processArticleListFromFile("countries.txt")
fout=codecs.open("data_output_composers.txt","w",encoding="utf-8")
processArticleListFromFile("german_composers.txt")
fout.close()

pass
