﻿from BeautifulSoup import BeautifulSoup
##import xml.etree.cElementTree as etree
import lxml.etree as etree

import sqlite3 as lite
from os.path import exists
import os
import urllib
import re
import time

from util import *

from nltk.tokenize.treebank import TreebankWordTokenizer

def unescapeHTML(text):
	html_escape_table = {"&amp;":"&" , "&quot;":'"' , "&apos;":"'","&gt;":">",
	"&lt;":"<","<br/>":"","<noinclude>":"","</noinclude>":""}
	for k in html_escape_table:
		text=text.replace(k,html_escape_table[k])
	return text

def removeMarkup(text,match,mk1,mk2):
	ibpos=text.find(match)
	while ibpos > -1:
		open_mk=1
		cnt=ibpos+len(match)
		while cnt < len(text) and open_mk > 0:
			if text[cnt:cnt+len(mk1)]==mk1:
				open_mk+=1
			if text[cnt:cnt+len(mk2)]==mk2:
				open_mk-=1
			cnt+=1

		ibendpos=cnt
		if open_mk==0:ibendpos+=1
		#text.find(mk2,ibpos+len(match)-1)
		if ibendpos > -1:
			text=text[:ibpos]+text[ibendpos+len(mk2)-1:]
		ibpos=text.find(match)
	return text

def stripWikiMarkup(text):
	""" very simple removal of wiki markup. Proper tokenization into tuples (word,uri) done by tokenizeWikiMarkup()"""
	print "original: ",text
	links=[]
	ibpos=text.find("[[")
	while ibpos > -1:
		ibendpos=text.find("]]",ibpos+2-1)
		if ibendpos == -1:
			ibendpos = len(text)
		pipepos=text.find("|",ibpos+2-1,ibendpos)
		if pipepos > -1:
			text=text[:ibpos]+text[pipepos+1:ibendpos]+text[ibendpos+2:]
		else:
			text2=text[:ibpos]+text[ibpos+2:ibendpos]
			if ibendpos < len(text):
				text2+=text[ibendpos+2:]
			text=text2
		ibpos=text.find("[[")

	text=text.strip()
	print "modified: ",text
	return text

all_numbers=[str(c) for c in range(10)]

def onlyNumbersInString(s):
	for char in s:
		if char not in all_numbers:
			return False

	return True


def scrapeArticleText(text):

##	text=removeMarkup(text,"{{Taxobox","{{","}}")
##	text=removeMarkup(text,"{{otheruses","{{","}}")
##	text=removeMarkup(text,"{{Image","{{","}}")

##	text=removeMarkup(text,"{{complex","{{","}}")
##	text=removeMarkup(text,"{{dablink","{{","}}")
##	text=removeMarkup(text,"{{dablink","{{","}}")
##	text=removeMarkup(text,"{{about","{{","}}")
##	text=removeMarkup(text,"{{IPA","{{","}}")
##	print "1:",text
	while text.startswith(":''") or text.startswith(": ''"):
		pos=text[3:].find("''")
		if pos != -1:
			text=text[4+pos+2:]

	text=text.replace("<onlyinclude>","")
	text=text.replace("</onlyinclude>","")
	text=removeMarkup(text,"{{{","{{{","}}}")
	text=removeMarkup(text,"{{formatnum","{{","}}")

	text=removeMarkup(text,"{{Infobox","{{","}}")
##	print "2:",text
	text=removeMarkup(text,"{{","{{","}}")
##	print "3:",text
	text=removeMarkup(text,"{|","{|","|}")
##	print "4:",text

	text=removeMarkup(text,"[[File:","[[","]]").strip()
##	print "4:",text
	text=removeMarkup(text,"[[Image:","[[","]]").strip()
##	text=removeMarkup(text,"<ref","<",">").strip()
##	text=removeMarkup(text,"</ref","<",">").strip()
	text=removeMarkup(text,"<!--","<!--","-->").strip()

	text=re.sub(r"''+","\"",text)
	text=re.sub(r"<ref>.*?</ref>"," ",text)
	text=re.sub(r"<div.*?</div>"," ",text)

	text=unescapeHTML(text)

##	text=removeMarkup(text,"(pronunciation","(",")")


##	text=removeMarkup(text,"(","(",")")
##	print "5:",text
##	if len(text) > 0:
##		if text[0]in "]:}" :text=text[1:]
	return text

def convertXMLtoSQLite(xml,sqlite):
	if not exists(xml):
		raise Exception
		return
	if exists (sqlite):
		os.remove(sqlite)
##		raise Exception
##		return

	conn=lite.connect(sqlite)
	conn.text_factory=str
	c=conn.cursor()
	c.execute("create table articles (wiki_link text, title text, article_text text)")
	conn.commit()

	tree=etree.parse(xml)
	root=tree.getroot()
	sch="{http://www.mediawiki.org/xml/export-0.6/}"
	print root.tag
	print len(root)
##	for cnt in range(len(root)):
##		print root[cnt].tag
##	print root.find(sch+"siteinfo")
	for page in root.iterfind(sch+"page"):

		if page.find(sch+"ns").text != "0":     # ignore anything not in ns 0 -  Why?
			continue

		if page.find(sch+"redirect") != None:   # ignore redirects
			continue


		title=page.find(sch+"title").text

		if onlyNumbersInString(title): continue

		if title.lower().startswith("list of"): continue # ignore lists

		text=page.find(sch+"revision").find(sch+"text").text

		if text.lower().find("{{disambiguation}}") != -1:  # ignore disambiguation pages
			continue

		if text.lower().find("{{disambig}}") != -1:  # ignore disambiguation pages
			continue


		# remove the lang: links at the end
		text=text.split("\n")
		cnt=len(text)-1
		while text[cnt].strip().lower().startswith("[["):
			cnt-=1

		text=text[:cnt]
		text=" ".join(text)
		text=scrapeArticleText(text)

##		try:
		text=text.encode("utf-8")
		wikititle=wikifyTitle(title)

		article_added=False
		trials=0
		while not article_added and trials < 3:
			trials+=1
			if trials > 1:
				print "Retrying..."
			try:
				c.execute("insert into articles (wiki_link,title,article_text) values (?,?,?)",(wikititle, title, text))
				conn.commit()
				print "Added article: ", title, "(",wikititle,")"
				article_added=True
			except:
				print "Error adding article", title
				time.sleep(0.3)

##		except:
##			print "Failed inserting article: ", title, "(",wikititle,")"
##			pass

	conn.close()


##convertXMLtoSQLite("c:\\NLP\\thesis\\simplewiki-latest-pages-articles.xml","c:\\NLP\\thesis\\simplewiki_articles_full2.db")
##convertXMLtoSQLite("c:\\NLP\\thesis\\simplewiki_test.xml","c:\\NLP\\thesis\\simplewiki_articles_test.db")

##print wikifyTitle("Baden-Württemberg")

s="""
{{Infobox country
|native_name =
|conventional_long_name = Commonwealth of Australia
|common_name = Australia
|image_flag = Flag of Australia.svg
|image_coat = Australian Coat of Arms.png
|image_map = Australia (orthographic projection).svg
|map_width = 220px
|national_anthem = "[[Advance Australia Fair]]"{{#tag:ref|Australia also has a [[royal anthem]], "[[God Save the Queen|God Save the Queen (or King)]]", which is played in the presence of a member of the [[House of Windsor|Royal family]] when they are in Australia. In all other appropriate contexts, the [[national anthem]] of Australia, "[[Advance Australia Fair]]", is played.<ref>[http://www.itsanhonour.gov.au/symbols/anthem.cfm It's an Honour&nbsp;– Symbols&nbsp;– Australian National Anthem] and [http://www.dfat.gov.au/facts/nat_anthem.html DFAT&nbsp;– "The Australian National Anthem"]; {{cite book|title=Parliamentary Handbook of the Commonwealth of Australia|edition=29th|year=2002 (updated 2005)|chapter=National Symbols|chapterurl=http://web.archive.org/web/20070611101901/http://www.aph.gov.au/library/handbook/40thparl/national+symbols.pdf|accessdate=7 June 2007}}</ref>|name="anthem explanation"|group="N"}}
|official_languages = None{{#tag:ref|English does not have ''[[de jure]]'' status.<ref name=language/>|name="official language"|group="N"}}
|languages_type = [[National language]]
|languages = [[English language|English]] (''[[de facto]]'')<ref name="official language" group="N" />
|capital = [[Canberra]]
|largest_city = [[Sydney]]
|government_type = [[Federalism|Federal]] [[Parliamentary system|parliamentary democracy]] and [[constitutional monarchy]]
|leader_title1 = [[Monarchy of Australia|Monarch]]
|leader_title2 = [[Governor-General of Australia|Governor-General]]
|leader_title3 = [[Prime Minister of Australia|Prime Minister]]
|leader_name1 = [[Elizabeth II]]
|leader_name2 = [[Quentin Bryce]]
|leader_name3 = [[Julia Gillard]]
|legislature = [[Parliament of Australia|Parliament]]
|upper_house = [[Senate of Australia|Senate]]
|lower_house = [[House of Representatives of Australia|House of Representatives]]
|area_rank = 6th
|area_magnitude = 1 E12
|area_km2 = 7617930
|percent_water =
|population_estimate = {{formatnum:{{#expr: 22766632 + (86400 / 91) * {{Age in days|2011|11|19}} round 0}}}}<!--AUTOUPDATES DAILY at 00:00 UTC, Australia pop clock adds 1 person every 91&nbsp;seconds --><ref>{{cite web|url=http://www.abs.gov.au/ausstats/abs@.nsf/94713ad445ff1425ca25682000192af2/1647509ef7e25faaca2568a900154b63?OpenDocument|title=Population clock|work=[[Australian Bureau of Statistics]] website|publisher=Commonwealth of Australia|accessdate=19 November 2011| archiveurl = http://www.webcitation.org/616hWn759 | archivedate = 2011-08-21| deadurl=no}} The population estimate shown is automatically calculated daily at 00:00 UTC and is based on data obtained from the population clock on the date shown in the citation.</ref>
|population_estimate_year = {{CURRENTYEAR}}
|population_estimate_rank = 50th
|population_census = 19,855,288<ref>{{Census 2006 AUS|id=0|name=Australia|accessdate=14 October 2008|quick=on}}</ref>
|population_census_year = 2006
|population_density_km2 = 2.8
|population_density_rank = 233rd
|sovereignty_type = Independence
|sovereignty_note = from the [[United Kingdom]]
|established_event1 = [[Constitution of Australia|Constitution]]
|established_event2 = [[Statute of Westminster 1931|Statute of Westminster]]
|established_event3 = [[Statute of Westminster Adoption Act 1942|Statute of Westminster Adoption Act]]
|established_event4 = [[Australia Act 1986|Australia Act]]
|established_date1 = 1 January 1901
|established_date2 = 11 December 1931
|established_date3 = 9 October 1942 (with effect from 3 September 1939)
|established_date4 = 3 March 1986
|currency = [[Australian dollar]]
|currency_code = AUD
|time_zone = [[Time in Australia|various]]<ref name="time" group="N">There are minor variations from these three time zones, see [[Time in Australia]].</ref>
|utc_offset = +8 to +10.5
|time_zone_DST = [[Time in Australia|various]]<ref name="time" group="N" />
|utc_offset_DST = +8 to +11.5
|demonym = [[Australians|Australian]], [[Aussie]]<ref>The [[Macquarie Dictionary]]</ref><ref>{{cite book|title=[[Collins English Dictionary]]|year=2009|publisher=HarperCollins|location=Bishopbriggs, Glasgow|isbn=978-0-00-786171-2|page=18|accessdate=19 April 2010}}</ref>
|drives_on = left
|cctld = [[.au]]
|calling_code = [[+61]]
|ISO_3166-1_alpha2 = AU
|ISO_3166-1_alpha3 = AUS
|ISO_3166-1_numeric = 036
|sport_code = AUS
|vehicle_code = AUS
|GDP_PPP_year = 2011
|GDP_PPP = $918.978 billion<ref name="imf2">{{cite web|http://www.imf.org/external/pubs/ft/weo/2011/02/weodata/weorept.aspx?sy=2009&ey=2016&scsm=1&ssd=1&sort=country&ds=.&br=1&c=193&s=NGDPD%2CNGDPDPC%2CPPPGDP%2CPPPPC%2CLP&grp=0&a=&pr1.x=84&pr1.y=12|title=Australia|work=IMF website|publisher=International Monetary Fund|location=Washington, D.C.|accessdate=5 November 2011}}</ref>
|GDP_PPP_rank = 18th
|GDP_PPP_per_capita = $40,836<ref name=imf2/>
|GDP_PPP_per_capita_rank = 12th
|GDP_nominal = $1.507 trillion<ref name=imf2/>
|GDP_nominal_rank = 13th
|GDP_nominal_year = 2011
|GDP_nominal_per_capita = $66,983<ref name=imf2/>
|GDP_nominal_per_capita_rank = 5th
|Gini = 30.5<ref>{{cite web|url=https://www.cia.gov/library/publications/the-world-factbook/fields/2172.html|title=Distribution of family income&nbsp;– Gini index|work=The World Factbook|publisher=CIA|accessdate=2009-09-01| archiveurl = http://www.webcitation.org/5rRcwIiYs | archivedate = 2010-07-23| deadurl=no}}</ref>
|Gini_category = <span style="color:#fc0;">medium</span>
|Gini_year = 2006
|HDI_year = 2011
|HDI = {{increase}} 0.929<ref>{{cite web|url=http://hdr.undp.org/en/media/HDR_2011_EN_Table1.pdf|title=Human Development Report 2011|publisher=United Nations|accessdate=2011-11-02}}</ref>
|HDI_rank = 2nd
|HDI_category = <span style="color:#090;">very&nbsp;high</span>
}}
'''Australia''' is a [[country]] in the [[Southern Hemisphere]] between the [[Pacific Ocean]] and the [[Indian Ocean]].  Its official name is the '''Commonwealth of Australia'''. Australia is the sixth biggest country in the world by landmass. The Australian [[emblem]] is a flower called the Golden Wattle.

22 million people live in Australia, and about 80% of them live on the east coast. About 60% live in and around the mainland state capitals of [[Sydney]], [[Melbourne]], [[Brisbane]], [[Perth, Western Australia|Perth]] and [[Adelaide]]. Australia's capital city is [[Canberra]].

Australia, [[New Zealand]], [[New Guinea]] and other islands on the Australian [[tectonic plate]] are together called [[Australasia]]. They form one of the world's great [[ecozone]]s. When other Pacific islands are included, the term is [[Oceania]].

"""
print scrapeArticleText(s)


##root = etree.Element("root", interesting="totally")
##etree.SubElement(root,"child1")
##etree.SubElement(root,"child2").text="adsfasdf"
##
##etree.SubElement(root,"child3")
##print etree.tostring(root, pretty_print=True)