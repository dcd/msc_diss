import datetime
import dateutil
import re

txt='Bla [[June 25, 1767]] bla March 14, 1681 bla December 17, 1770 bla March 26, 1827  January 27, 1756    December 5, 1791'
txt+='  [[March 26]] [[1827]]  7 May 1833 [[5 June]], [[1826]] [[5 June]], [[1826]] 23 February 1685 14 April 1759'


repldict={"January":1,"Jan":1,"February":2,"Feb":2,"March":3,"Mar":3,"April":4,"Apr":4,"May":5,"June":6,
"Jun":6,"July":7,"Jul":7,"August":8,"Aug":8,"September":9,"Sept":9,"October":10,"Oct":10,"November":11,"Nov":11,"December":12,"Dec":12}

##    regExDate=r"(?i)((?:(?:[0-2]?\d{1})|(?:[3][01]{1})))(?![\d])(\s+)((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))(|,)(\s+)((?:(?:[1]{1}\d{1}\d{1}\d{1})|(?:[2]{1}\d{3})))(?![\d])|(((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))(\s+)((?:(?:[0-2]?\d{1})|(?:[3][01]{1})))(?![\d])(|,)(\s+)((?:(?:[1]{1}\d{1}\d{1}\d{1})|(?:[2]{1}\d{3})))(?![\d]))"
regExDate1=r"((?:(?:[0-2]?\d{1})|(?:[3][01]{1})))(?![\d])(\s+)((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))(|,)(\s+)((?:(?:[1]{1}\d{1}\d{1}\d{1})|(?:[2]{1}\d{3})))(?![\d])"
regExDate2=r"((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))(\s+)((?:(?:[0-2]?\d{1})|(?:[3][01]{1})))(?![\d])(|,)(\s+)((?:(?:[1]{1}\d{1}\d{1}\d{1})|(?:[2]{1}\d{3})))(?![\d])"
# and now with [[ ]]:
regExDate3=r"(\[{2})((?:(?:[0-2]?\d{1})|(?:[3][01]{1})))(?![\d])(\s+)((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))(|\]{2})(|,)(\s+)(|\[{2})((?:(?:[1]{1}\d{1}\d{1}\d{1})|(?:[2]{1}\d{3})))(\]{2})(?![\d])"
regExDate4=r"(\[{2})((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Sept|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?))(\s+)((?:(?:[0-2]?\d{1})|(?:[3][01]{1})))(?![\d])(|\]{2})(|,)(\s+)(|\[{2})((?:(?:[1]{1}\d{1}\d{1}\d{1})|(?:[2]{1}\d{3})))(\]{2})(?![\d])"

date1=re.compile(regExDate1,re.IGNORECASE|re.DOTALL)
date2=re.compile(regExDate2,re.IGNORECASE|re.DOTALL)
date3=re.compile(regExDate3,re.IGNORECASE|re.DOTALL)
date4=re.compile(regExDate4,re.IGNORECASE|re.DOTALL)

def normalizeDatesWithRegEx(text):
    replist=[]
    for x in date3.finditer(text):
        sd="%s-%d-%s"%(x.group(9),repldict[x.group(4)],x.group(2))
        #d=datetime.date(int(x.group(6)), repldict[x.group(1)], int(x.group(3)))
        replist.append((x.group(0),sd))

    for x in date4.finditer(text):
        sd="%s-%d-%s"%(x.group(9),repldict[x.group(2)],x.group(4))
        #d=datetime.date(int(x.group(6)), repldict[x.group(1)], int(x.group(3)))
        replist.append((x.group(0),sd))

    for g,d in replist:
        text=text.replace(g,"$date(%s)"%d)

    replist=[]

    for x in date1.finditer(text):
##        d=datetime.date(int(x.group(6)), repldict[x.group(3)], int(x.group(1)))
        sd="%s-%d-%s"%(x.group(6),repldict[x.group(3)],x.group(1))
        replist.append((x.group(0),sd))

    for x in date2.finditer(text):
        sd="%s-%d-%s"%(x.group(6),repldict[x.group(1)],x.group(3))
        #d=datetime.date(int(x.group(6)), repldict[x.group(1)], int(x.group(3)))
        replist.append((x.group(0),sd))

    for g,d in replist:
        text=text.replace(g,"$date(%s)"%d)

    return text


print normalizeDatesWithRegEx(txt)

