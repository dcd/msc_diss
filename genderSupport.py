#-------------------------------------------------------------------------------
# Name:        genderSupport
# Purpose:
#
# Author:      Daniel Duma
#
# Created:     05/08/2012
# Copyright:   (c) Daniel Duma 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

import cPickle
from rdf import *

genders_dict={}
genders_loaded=False

def getGenderFromSparqlJSON(wiki_link):
	res=[]
	query="""
SELECT ?obj
 WHERE {
 <%s> <%s> ?obj .

}
 """ % (fullTripleSubject(wiki_link),"http://xmlns.com/foaf/0.1/gender")
#FILTER (langMatches(lang(?obj),"") || langMatches(lang(?obj),"en") )
	data=""
	while data=="":
		data=sparqlQuery(query, "http://localhost:8080/openrdf-sesame/repositories/dbpedia")

	if isinstance(data,basestring):
		print data
		return ""

	d =data.items()[1][1]['bindings'][0]
	obj=d.items()[0][1]
	if obj["type"]=="literal":
		if "xml:lang" in obj:
			lng=obj["xml:lang"]
		else:
			lng=""
	else:
		if "xml:lang" in obj:
			lng=obj["xml:lang"]
		else:
			lng=""


	return obj["value"]


def getGenderFromSparql(wiki_link):
	res=[]
	query="""
SELECT ?obj
 WHERE {
 <%s> <%s> ?obj .

}
 """ % (fullTripleSubject(wiki_link),"http://xmlns.com/foaf/0.1/gender")
#FILTER (langMatches(lang(?obj),"") || langMatches(lang(?obj),"en") )
	data=""
	while data=="":
		data=sparqlQuery(query, factforge_endpoint)

	if "male" in data:
		return "male"

	if "female" in data:
		return "female"

	return ""

def getGenderByAllMeans(subj):
	gender=""
	gender=getGenderFromSparql(subj)
	if gender=="":
		genders_dict=loadGenders()
		if subj in genders_dict:
			gender=genders_dict[subj]
	return gender

def loadGenders():
	global genders_loaded
	global genders_dict
	if genders_loaded: return genders_dict
	genders_dict=cPickle.load(file(r"C:\NLP\thesis\genders.pickle"))
	genders_loaded=True
	return genders_dict
