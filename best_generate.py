#-------------------------------------------------------------------------------
# Name:		bl_generate
# Purpose:	  Baseline generation
#
# Author:	  Daniel Duma
#
# Created:	 07/06/2012
# Copyright:   (c) Daniel Duma 2012
# Licence:	 <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from util import *
from rdf import *
import cmath
from genderSupport import *

class iterFill:
	def __init__(self):
		self.varnum=0
		self.graphPaths=[]
		self.values=[]

class generateRec:
	def __init__(self,wiki_link):
		self.discourse_model=defaultdict(int)
		self.full_subj=fullTripleSubject(wiki_link)
		self.wiki_link=wiki_link
		self.classes=[]
		self.basicRE=getBasicSelfRE(wiki_link) # basic self- referring expression, i.e. rdfs_label/foaf:name
		self.triples=[]
		self.triples_used=defaultdict(int)
		self.gp_values={}
		self.found_gps=[]
		self.pronoun="it"
		self.trained_model=None
		self.sentence_num=0
	def __repr__(self):
		res="<generateRec (wiki_link:"+self.wiki_link+")>"
		return res
	def startGen(self):
		self.sentence_num=0
		self.discourse_model=[]


def getREG(model,entity,surface):
	"""
	Return a Referring Expression for "entity", given "surface" features and the "model"
	"""
	if entity not in model.discourse_model:
		model.discourse_model[entity]+=1
		if entity==self_pointer:
			if token_gen in surface:
				return model.basicRE+"'s"
			else:
				return model.basicRE

	if entity==self_pointer:
		if token_gen in surface:
			return posessives[model.pronoun][0]
		elif token_nom in surface:
			return model.pronoun

def tt_stringHash(s):
	s=re.sub(r"[.,\-_\\/]"," ",s)
	words=s.split()
	words=list(set(words))
	words=sorted(words)
	h=" ".join(words)
	return h

def tt_excludeDuplicates2(candidates):
	"""
	in: candidates = list of (string,predicate)
	"""
	hashes={}
	keep=[]
	for c in candidates:
		h=tt_stringHash(c)
		if h not in hashes:
			hashes[h]=[]
		if c not in hashes[h]:
			hashes[h].append(c)

	for h in hashes:
		keep.append(chooseBestSRforSlot(hashes[h]))
	return keep


def getValueForPredFromPools(model, gp):
	pool=findPathInPools(model,gp)
	if pool:
		return pool.values
	return None

def getGraphPathValue(graphPath,self_value):
	"""
	resolve a graphPath (a string like $self\bla1\bla2) into a value recursively
	automatically replaces $self with actual triple subject

	in -> triple = string, self_value = value to set $self mask to
	out -> list of rdfTriple
	"""
	graphPath=expandStringNamespaces(graphPath)
	bits=graphPath.replace(self_pointer,self_value)
	bits=bits.split(gp_sep)
	res=""

	if len(bits) == 1:
		bits.append(self_REG_default)
##	else:
##		if bits[-1]

	res=quickTripleQuery(bits[0],bits[1])
	if len(res) == 0:
		return []

	if len(bits) > 2:
		for cnt in range(2,len(bits)):
			val=fullTripleSubject(wikifyTitle(res[0].value))
			res=quickTripleQuery(val,bits[cnt])
			if len(res) == 0:
				return []

	return res

def recursiveIterate(template,iters,pos,varnum=0):
	res=[]
	if pos == len(iters): return []

	for val in iters[pos].values:
		varnum+=1
		tmp=template.replace("$[x"+str(pos+1)+"]$",str(val))
		if pos < len(iters) -1:
			res.extend(recursiveIterate(tmp,iters,pos+1,varnum))
		else:
			res.append(tmp)

	return res


def getPathTrail(tp):
	pos=tp.rfind(gp_sep)
##	if pos == -1: pos=0
	return tp[pos+1:]

def splitPathTrail(tp):
	pos=tp.rfind(gp_sep)
	if pos == -1: pos=0
	return tp[pos+1:],tp[:pos]


def filledTemplateToString(temp):
	res=u""
	cnt=1
	for t in temp:
		if isinstance(t,basestring):
			res+=t+" "
		if isinstance(t,dict):
			res+=u"$[x"+unicode(cnt)+u"]$ "
			cnt+=1
	return res


def fillInTemplate(gen_model,template):
	filled_template=[]
	varnum=0

	for token in template:
		if isinstance(token,basestring):
			filled_template.append(token)
		if isinstance(token,templateSlot):
			if self_pointer in token:
				#REG
				text=getREG(gen_model,self_pointer,token.surface)
				filled_template.append(text)
			else:
				for gp in token:
					if extractPredFromGP(gp) not in [rdf_type]:
						pool=findPathInPools(gen_model.trained_model,gp)
						if len(pool.values) > 0:
							surface_values=[]
							for val in pool.values:
								if is_URI(val):
									surface_values.append(getLabelFor(val))
								elif is_rdf_date(val):
				##					print "is date!!!!!"
									surface_values.append(surfaceDate(val))
								else:
									surface_values.append(val)

							surface_values=tt_excludeDuplicates2(surface_values)

							if extractPredFromGP(pool.name) in self_corefering_predicates or extractPredFromGP(gp) in self_corefering_predicates:
								gen_model.discourse_model[self_pointer]+=1

##							if token_list in pool.surface:
							if len(surface_values) > 1 and extractPredFromGP(pool.name) not in self_corefering_predicates:
								text=tt_stringFromList(surface_values)
							else:
								text=chooseBestSRforSlot(surface_values)
##							text=tt_stringFromList(surface_values)

							filled_template.append(text)
							break
					else:
						label=getLabelFor(gen_model.trained_model.name)
						label=makePluralSingular(label)
						filled_template.append(label)

	return filled_template

def getSentenceTokenDensity(sp):
	return len([x for x in sp if isinstance(x,list)]) / float(len(sp))

def chooseBestSRforSlot(candidates):
	if len(candidates) == 0: return "",[]
	scores=[]
	for c in candidates:
		pun=0
		if re.match(r"[.,]",c[0]):pun+=1
		score=len(c[0])*pow(2,pun)
		scores.append((c,score))
	a,b=sorted(scores,key=lambda x:x[1],reverse=False)[0]
##	print "Of candidates",candidates
##	print "The chosen is",a[0],", from predicates", a[1]

	return a

def renderSentence(sent):
	res=""
	for s in sent:
		res+=codecs.decode(s, "utf-8")+u" "
	res=res.strip()
	res=res.replace("-LRB-","(").replace("-RRB-",")")
	res=res.replace("( )","")
	if len(res) > 0:
		if res[-1]!=".": res +="."
		res=res[0].upper()+res[1:]
	res=re.sub(r"\s+([.,;])",r"\1",res)
	res=re.sub(r"\(\s(.*)\s\)",r"(\1)",res)
	res=re.sub(r"one of the",r"a",res) # one of the -> a
	res=re.sub(r"\s(a)\s([aeiouwAEIOUW])",r" an \2",res) # a/an
	res=re.sub(r"\s+",r" ",res) # a/an
	res=re.sub(r",,",r",",res) # a/an
	res=re.sub(r",\.",r".",res) # a/an
	res=re.sub(r",\)",r")",res) #
	res=re.sub(r"\san\s([bcdfghjklmnpqrstvxyz])",r" a \1",res) # a/an
	return res


def sentenceFulfillsRequirements(gen_model,s):
	"""
	Return True if there are enough predicates to instantiate the sentence
	"""
	for t in s:
		if isinstance(t,templateSlot):
			full=False
			for path in t:
				for p in gen_model.trained_model.pools:
					if len(p.values) > 0 and str(path) in p:
						full=True
						break
			if not full:
				return False
	return True

def enoughParametersForSentence(available_paths,s):
	import copy
	paths_copy=copy.deepcopy(available_paths)
	for t in s:
		if isinstance(t,list):
			full=False
			for path in t:
				if paths_copy[path] > 0:
					paths_copy[path]-=1
					full=True
			if not full:
				return False
	return True


def getClassTriplesForModel(gen_model):
	gen_model.triples=quickTripleQuery(gen_model.full_subj,rdf_type,self_pointer)
	gen_model.classes=[t.value for t in gen_model.triples]


def getTriplesForModel(gen_model):
##	model.triples=getTriplesUsingCache(wiki_link)
##	model.triples=overrideTriplesSubject(selectTriplesLanguage(model.triples,"EN"),self_pointer)
##	model.triples=shortenTripleNamespaces(model.triples)
	for gp in gen_model.trained_model.ngrams[1]:
		gp=gp[0]
		found_triples=getGraphPathValue(gp,gen_model.full_subj)
##		trail, path=splitPathTrail(gp)
		if len(found_triples) > 0:
			lst=gen_model.gp_values.setdefault(gp,[])
			for v in found_triples:
				lst.append(v.value)

def getTriplesForModelPools(gen_model):
	for pool in gen_model.trained_model.pools:
		found_triples=[]
		pool.values=[]
		for gp in pool:
			found_triples=getGraphPathValue(gp,gen_model.full_subj)
	##		trail, path=splitPathTrail(gp)
			if len(found_triples) > 0:
				pool.values.extend([v.value for v in found_triples])
##				for gp2 in pool:
##					lst=gen_model.gp_values.setdefault(gp2,[])
##     				for v in found_triples:
##						lst.append(v.value)
				break


def instantiatePredicateScore(model, triples):
	"""
	Returns the score of a
	"""
	res=0
	preds=[]
##	print "\n",model.name
	for t in triples:
		preds.append(t.asGraphPath())

	preds=list(set(preds))

	for t in preds:
		for p in model.pools:
			if t in p:
##				print t
				res+=1
				break
	return res

def instantiatePredicateAndSentenceScore(model, triples):
	"""
	Returns the score of a
	"""
	res=0
	inst=[]
	preds=[]
##	print "\n",model.name
	for t in triples:
		preds.append(t.asGraphPath())

	preds=list(set(preds))

	for pred in preds:
		pool=findPathInPools(model,pred)
		if pool is None:
			continue
		for s in model.sentences:
			for token in s:
				if isinstance(token,templateSlot):
					if pool.name in token:
						if pred not in inst:
							inst.append(pred)
		##				print t
							res+=1
						break

	return len(inst)

def getNbestClasses(gen_model,max_classes):
	words=defaultdict(int)
	classes=[]
	for c in gen_model.classes:
		s=space_out_camel_case(extractClassName(c)).split() # retrieve rdfs:label instead
		numwords=len(s)
##		print " ".join(s).strip()
		s=removeStopWords(s)

		for w in s:
			words[w]+=1
		classes.append((c,s,numwords)) # save the class, its

	scores=[]
	for tp in classes:
		score=0.0
		for word in tp[1]:
			score+=words[word]
		divide=float(tp[2])
		if tp[2]==1:
			divide=5
		score/=divide # normalise the score by the word length
		scores.append((tp[0],score))

	if len(scores) > 0:
		return sorted(scores,key=lambda x:x[1],reverse=True)[:max_classes]
	return []

def selectClassToGenerateFrom(gen_model,classModels, triples):
	"""
	Uses how many predicates would be instantiated and tf score
	"""

	words=defaultdict(int)
	classes=[]
	for c in gen_model.classes:
		s=getLabelFor(c).split()
		numwords=len(s)
		s=removeStopWords(s)

		for w in s:
			words[w]+=1
		classes.append((c,s,numwords))

	scores=[]
	for tp in classes:
		if tp[0] in classModels:
			score=instantiatePredicateAndSentenceScore(classModels[tp[0]],triples) / float(2)
		else:
			score=0

		tfscore=0
		for word in tp[1]:
			tfscore+=words[word]/float(10)
		divide=float(tp[2])
		if divide==1:divide=5
		tfscore/=divide # normalise the score by the word length
##		score-=abs(2-tp[2])
##		if tp[2]==1: score-=5
		scores.append((tp[0],score, tfscore))

	for c,score,tfscore in sorted(scores,key=lambda x:x[1],reverse=True):
##		print c,score
		if c in classModels:
			return c

	return ""

def selectClassToGenerateFrom2(gen_model,classModels, triples):
	"""
	Uses how many predicates would be instantiated and tf score
	"""

	nbest=getNbestClasses(gen_model,5)
	print nbest
	scores=[]
	for tp in nbest:
		if tp[0] in classModels:
			score=instantiatePredicateAndSentenceScore(classModels[tp[0]],triples) / float(2)
		else:
			score=0

		scores.append((tp[0],score))

	for c,score in sorted(scores,key=lambda x:x[1],reverse=True):
##		print c,score
			return c

	return ""


def batchPprint(filename,models,num=100):
	try:
		f=codecs.open(filename,"r",encoding="utf-8")
	except:
		print "File not found:", filename
		return

	cnt=0

	for line in f :
		cnt+=1
		if num > 0 and cnt > num:
			break

		line=line.strip("\n\r ").replace(">","").replace("<","")
		if line=="":
			continue
		pprintTriplesFor(classModels,line)
		print
	f.close()

def countPoolsUsed(model,combi):
	"""
	Returns the number of pools that get used by a combination
	"""
	used_pools=[]
	for sent in combi:
		for token in sent:
			if isinstance(token,templateSlot):
				for gp in token:
					pool=findPathInPools(model,gp)
					if pool not in used_pools:
						used_pools.append(pool)

	return len(used_pools)

def enoughTriplesToFill(model,combi):
	for sent in combi:
		for token in sent:
			if isinstance(token,templateSlot):
				full=False
				for gp in token:
					if gp != self_pointer:
						pool=findPathInPools(model,gp)
						if len(pool.values) > 0:
							full=True
					else:
						full=True
				if not full:
					return False
	return True

def buildTemplateFromPredicate(pred,verb,value):
	"""
	Build a simple sentence template, as used by the chart generation
	"""
	subj=templateSlot()
	subj.append(self_pointer)
	subj.coref=self_pointer
	subj.surface.append(token_gen)
	obj=templateSlot()
	obj.append(value)

	res=[]
	res.append(subj)
	res.append(pred)
	res.append(verb)
	res.append(obj)
	return res

def buildTypeTemplate(value):
	"""
	Build a simple sentence template, as used by the chart generation
	"""
	subj=templateSlot()
	subj.append(self_pointer)
	subj.coref=self_pointer
	subj.surface.append(token_nom)
	obj=templateSlot()
	obj.append(value)

	res=[]
	res.append(subj)
	res.append("is a")
	res.append(obj)
	return res


def tt_stringFromList(lst):
	if len(lst) == 0: return ""
	if len(lst) == 1: return lst[0]

	res=""

	for i,e in enumerate(lst):
		res+=e
		if i == len(lst)-2:
			res+=" and "
		elif i== len(lst)-1:
			pass
		else:
			res+=", "
	return res

def listOfValues():
	"""
	Generate a list from a property with multiple values
	"""
	values=[]
	for val in triples[t]:
		if is_URI(val):
			values.append(getLabelFor(val))
		elif is_rdf_date(val):
	##					print "is date!!!!!"
			values.append(surfaceDate(val))
		else:
			values.append(val)
	obj=tt_stringFromList(values)




def addSentencesForOrphansToChart(chart, gen_model):
##	orphans=getOrphanPools(gen_model.trained_model)
	orphans=[]
	for pool in gen_model.trained_model.pools:
		if len(pool.values) > 0 and getPathTrail(pool.name) not in self_corefering_predicates:
			orphans.append(pool)

	for o in orphans:
		triple_pred=getPathTrail(o.name)
		if triple_pred in self_corefering_predicates:
			continue
		if triple_pred == rdf_type:
			template=buildTypeTemplate(buildGraphPath([self_pointer, rdf_type]))
			chart.append(template)
			continue
		pred=getLabelFor(triple_pred).lower()

		if isNumberAndIsLessThan(o.values[0],30):
			# skip property if under x
			continue

		if o.values[0][-4:] in [".jpg",".png",".tga",".svg",".bmp"]:
			continue

		if len(o.values[0].split()) > 10: # if the firstvalue has more than x words, skip the triple
			continue

		verb="is"
##		if len(o.values) > 1 and token_list in o.surface:
		if len(o.values) > 1 and extractPredFromGP(o.name) not in self_corefering_predicates:
			verb="are"
			pred=makeSingularPlural(pred)

		if pred.startswith("is") or pred.startswith("has"):
			words=pred.split()
			part2=" ".join(words[1:]).strip()
			part1=words[0]+" a "
			pred=part1+part2
			verb=":"
			subj=gen_model.pronoun
		elif pred.startswith("was"):
			verb=""
		elif pred.startswith("known"):
			verb="is"
		else:
			subj=posessives[gen_model.pronoun][0]

		template=buildTemplateFromPredicate(pred, verb, o.name)
		chart.append(template)

def forwardNgramScore(gen_model,ngrams_so_far,sentence, max_consider=20):
	ngramlist=ngrams_so_far[:]

	count=0
	for token in sentence:
		if isinstance(token,templateSlot):
			ngramlist.append(token)
			count+=1
			if count >= max_consider: break

	total_score=1
	for x,n in enumerate(ngramlist[3:]):
##		for i in range(1,maxNgram+1):
			i=2
			y=x+3+1
			nglist=iterateBuildNgrams(ngramlist[y-i:y],0)
			highest=0
			for ng in nglist:
				ng=tuple(ng)
				if gen_model.trained_model.ngrams[i][ng]> highest:
					highest=gen_model.trained_model.ngrams[i][ng]
			if highest==0: highest=0.00000001
			total_score*=highest
	return total_score

def poolInstantiateScore(sentence):
	score=0
	for token in sentence:
		if isinstance(token,templateSlot):
			for gp in token:
				pred=extractPredFromGP(gp)
				if pred not in self_corefering_predicates:
					score+=1
					break
	return score

def getUsedPools(sentence, model):
	res=[]
	for token in sentence:
		if isinstance(token,templateSlot):
			for gp in token:
				pred=extractPredFromGP(gp)
				if pred not in self_corefering_predicates:
					pool=findPathInPools(model, gp)
					if pool is not None:
						res.append(pool)
					break
	return res

def usesMorePoolsThanAllowed(used_pools):
	for pool in used_pools:
		if pool.issued >= pool.count_per_article:
			return True
	return False

def buildCombinationViterbi(chart, gen_model):
	combination=[]
	ngrams=["","",""]

	while len(combination) < len(chart):
		considered=[]
		for sent in chart:
			if sent not in combination:
				used_pools=getUsedPools(sent,gen_model.trained_model)
				if usesMorePoolsThanAllowed(used_pools):
					continue
				ngram_score=forwardNgramScore(gen_model,ngrams,sent,1)
				score=ngram_score + ((len(used_pools)) * ngram_score * 0.01)
				considered.append((sent,score,used_pools))

		if len(considered)==0:
			break

		for sent,score, used_pools in sorted(considered,key=lambda x:x[1],reverse=True):
			print "Adding: ", sent
			combination.append(sent)
			for p in used_pools:
				ngrams.append(p.name)
				p.issued += 1
			break

	return combination



def loadModelDict(filename):
	print "Using model: ",filename
	f=open(filename,"r")
	res=cPickle.load(f)
	return res

def countRealisedPredicates(model,triples):
	res=0
	for t in triples:
		if t.pred in model.ngrams[1]:
			res+=1
	return res

def pprintTriplesFor(classModels,wiki_link):
	gen_model=generateRec(wiki_link)
	getClassTriplesForModel(gen_model)
	triples=filterOutUselessTriples(getTriplesUsingCache(wiki_link,self_pointer))

	cls=selectClassToGenerateFrom(gen_model,classModels, triples)
	if cls=="":
		print "Cannot find a single class to generate from: ",gen_model.classes
##			assert 1==0
		return
	else:
		classModel=classModels[cls]
		print "Using model:",cls

	gen_model.trained_model=classModel

	getTriplesForModelPools(gen_model)
	print "\n\n"
	print "Category:",getLabelFor(cls)
	for p in gen_model.trained_model.pools:
		if len(p.values) > 0:
			pred=extractPredFromGP(p[0])
			if pred not in [rdf_type] and pred not in useless_predicates:
				print getLabelFor(pred).lower(), "=",
				for val in p.values:
					surface=val
					if is_URI(val):
						surface=getLabelFor(val)
					print surface+",",
				print

##	print "Sentences templates:"
##	for s in gen_model.trained_model.sentences:
##		print s
##	assert 1==0
	pass

def manualAddClassAsPool(model, chosen_class):
	p=predPool(buildGraphPath([self_pointer, rdf_type]))
	p.append(p.name)
	p.count_per_article=1
	p.values.append(chosen_class)
	model.pools.append(p)

def generateArticle(classModels,wiki_link,article_class=None):
	"""
	generate the text for one article identified by (wiki_link)

	"""
	print "Generating article:"
	gen_model=generateRec(wiki_link)
	getClassTriplesForModel(gen_model)

	triples=filterOutUselessTriples(getTriplesUsingCache(wiki_link,self_pointer))

	if article_class is not None:
		if article_class not in classModels:
			print "Cannot find article class",article_class
			return
		classModel=classModels[article_class]
	else:
		cls=selectClassToGenerateFrom2(gen_model,classModels, triples)
		if cls=="":
			print "Cannot find a single class to generate from: ",gen_model.classes
##			assert 1==0
			return
		else:
			classModel=classModels[cls]
			print "Using model:",cls

	gen_model.trained_model=classModel

	gender=getGenderByAllMeans(gen_model.full_subj)
	if gender=="male":gen_model.pronoun="he"
	elif gender=="female":gen_model.pronoun="she"
	else:
		gen_model.pronoun=classModel.bestPronoun()
		if gen_model.pronoun=="": gen_model.pronoun="it"

	getTriplesForModelPools(gen_model)
	manualAddClassAsPool(gen_model.trained_model,cls)

	chart=[]

	for s in gen_model.trained_model.sentences:
		if sentenceFulfillsRequirements(gen_model,s):
			chart.append(s)

	addSentencesForOrphansToChart(chart,gen_model)

	cur_list=[]

	print "chart: ", chart
	print
	combination=buildCombinationViterbi(chart,gen_model)
##	print "combination:", combination
	print "\n\nArticle:"
	for sent in combination:
		filled=fillInTemplate(gen_model, sent)
		print renderSentence(filled),


##classModels=loadModelDict(r"c:\nlp\thesis\bl_model_1.mod")
##generateArticle(classModels,"Jennifer_Saunders")
##classModels=loadModelDict(r"c:\nlp\thesis\bl_model_2.mod")
##generateArticle(classModels,"Hyundai_Motor_Company")
##classModels=loadModelDict(r"c:\nlp\thesis\bl_model_3.mod")
##generateArticle(classModels,"Nicole_Scherzinger")
##classModels=loadModelDict(r"c:\nlp\thesis\bl_model_4.mod")
##generateArticle(classModels,"Morocco")
##classModels=loadModelDict(r"c:\nlp\thesis\bl_model_5.mod")
##generateArticle(classModels,"Woody_Woodpecker")
##classModels=loadModelDict(r"c:\nlp\thesis\bl_model_6.mod")
##generateArticle(classModels,"Real_Zaragoza")
##classModels=loadModelDict(r"c:\nlp\thesis\bl_model_7.mod")
##generateArticle(classModels,"Fernando_Gago")
##classModels=loadModelDict(r"c:\nlp\thesis\bl_model_8.mod")
##generateArticle(classModels,"American_Idol")
##classModels=loadModelDict(r"c:\nlp\thesis\bl_model_9.mod")
##generateArticle(classModels,"Mercyful_Fate")
##classModels=loadModelDict(r"c:\nlp\thesis\bl_model_10.mod")
##generateArticle(classModels,"William_Herschel")
##classModels=loadModelDict(r"c:\nlp\thesis\bl_model_11.mod")
##generateArticle(classModels,"Belgrade")
##classModels=loadModelDict(r"c:\nlp\thesis\bl_model_12.mod")
##generateArticle(classModels,"Winx_Club")


classModels=loadModelDict(r"c:\nlp\thesis\bl_model_5.mod")
print classModels["http://dbpedia.org/class/yago/FictionalAnthropomorphicCharacters"].sentences
print classModels["http://dbpedia.org/class/yago/FictionalAnthropomorphicCharacters"].pools

