#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      MasterMan
#
# Created:     01/06/2012
# Copyright:   (c) MasterMan 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

english_stopwords = """a an about after again against all also and another any around as ask
 at back because become before begin between both but by call can change child
 come consider could course day develop do down during each early end even eye face
 fact feel few find first for from get give go good great
 have he head help here high home house how however I if in increase
 interest into it just keep know large last late lead leave life like line little
 long look make man many may mean might more most move much must nation need never
new no not now number of off old on one only open or order other out over own part
right run same seem set she should show since small so some stand
state still such system take tell than that the then there these they thing think
this those through time to too turn under up use very want way we well what when
where which while who will with without would write year you
is are were be was has 's i you my your his her me us ."""

grammar_stopwords="""a an the one he she it we us him her its me you your my mine their theirs
of from to for with and or not
is was are were
# . , 's
"""


def extractWordsFromAlignment(text_tokens,wordnum,alignment_length=1):
    limit=min(wordnum+word_window_size,len(text_tokens))
    right_limit=limit
    for cnt in range(wordnum,limit):
        if text_tokens[cnt][1]==phrase_break_token:
            right_limit=cnt
            break

    limit=max(0,wordnum-word_window_size)
    left_limit=limit
    for cnt in range(wordnum,limit,-1):
        if text_tokens[cnt][1]==phrase_break_token:
            left_limit=cnt
            break

    res=[]
    for cnt in range(left_limit,right_limit):
        if cnt not in range(wordnum,wordnum+alignment_length):
            res.append(text_tokens[cnt][0])
        else:
            if len(res)==0 or res[-1] != self_word_token:
                res.append(self_word_token)

    return res

def extractContentWordsFromAlignment(text_tokens,wordnum,alignment_length=1):
    res=extractWordsFromAlignment(text_tokens,wordnum,alignment_length)

    res2=[]
    for r in res:
        r=r.lower()
        if r not in grammar_stopwords:
            res2.append(r)

    return res2


def main():
       for al in alignments:
        pred=al[0][0]
        wordnum=al[1]
        # store SLs
        pdict=rdf_sl_by_class.setdefault(article_class,{})
        sldict=pdict.setdefault(pred,{})

        sl=extractContentWordsFromAlignment(tokens,wordnum)
##        print sl
##        sl_s=listToString(sl)

        for word in sl:
            if word != ".":
                sldict[word]=sldict.get(word,0)+1

        # store RDFp for class
        pdict=rdf_by_class.setdefault(article_class,{})
        rdfrec=pdict.setdefault(pred,rdf_rec())
        rdfrec.appearsIn+=1
##        rdfrec.avgPos=min(rdfrec.avgPos,wordnum)
        rdfrec.avgPos+=wordnum