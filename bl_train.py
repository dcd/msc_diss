﻿# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------------
# Name:		 baseline_train
# Purpose:
#
# Author:	  Daniel Duma
#
# Created:	 03/04/2012
# Copyright:   (c) Daniel Duma 2012
#-------------------------------------------------------------------------------
#!/usr/bin/env python

import codecs
import operator
import sys
import re
import copy

##sys.stdout = codecs.getwriter('utf8')(sys.stdout)

from rdf import *
from alignments import *
from wiki import *
from coreference import *
from parser_interface import *

# GLOBAL TRAINING SETTINGS

PRUNE_PARSE=False		# Parse and prune the parse?
ADD_ROOT_CATEGORY=True  # if True, the term with the highest term frequency will be added as a yago:term class (e.g. Composers, Bands)
DICE_COEFFICIENT=0.7    # Dice coefficient at which predicates become synonymous
DICE_PURGE=0.1          # if below this coefficient, a template with conflicting predicates in its slots gets discarded
MAX_CLASSES=3           # Max number of classes to pick from n-best list, including Root Category if set to True

FO_DISCARDED_AFTER_PARSE="Disc AP"
FO_DISCARDED_DURING_PARSE="Disc DP"
FO_TEMPLATE_EXTRACTED="Template"

class statsRec:
	"""
	Record for collecting statistics of successful parses, etc.
	"""
	def __init__(self):
		self.total_articles=0
		self.total_sentences=0
		self.sent_considered=[]
		self.sent_discarded_during_parse=[]
		self.sent_discarded_after_parse=[]
		self.templates_extracted=[]
		self.sentences=[]

	def logSentenceOutcome(self,s,spottedEntities,finalOutcome,parseTree="-",prunedTree="-",template=""):
		d={}
		d["Sentence"]=s
		d["Spotted Entities"]=spottedEntities
		d["Outcome"]=finalOutcome
##		if isinstance(parseTree,basestring)
		d["Parse"]=unicode(parseTree)
		d["Pruned tree"]=str(prunedTree)
		d["Template"]=template
		self.sentences.append(d)

	def writeToCSV(self,filename):
		f=codecs.open(filename,"w",encoding="utf-8", errors="replace")
		if len(self.sentences) == 0: return
		f.write("\t".join(self.sentences[0].keys())+"\n")
		for d in self.sentences:
			f.write(d["Sentence"]+"\t"+str(d["Spotted Entities"])+"\t"+d["Outcome"]+"\t"+d["Parse"])
			f.write("\t"+d["Pruned tree"]+"\t"+d["Template"]+"\n")
		f.close()

classModels={}

stats=statsRec()

def orderClassesByTFwithText(tripleclasses,text_tokens):
	"""
	Given a list of classes, order them by their score based on term frequency
	"""
	words=defaultdict(int)
	classes=[]
	for c in tripleclasses:
		s=space_out_camel_case(extractClassName(c)).split() # retrieve rdfs:label instead
		numwords=len(s)
##		print " ".join(s).strip()
		s=removeStopWords(s)

		for w in s:
			words[w]+=1
		classes.append((c,s,numwords)) # save the class, its

# Deal with plurals?
##	text=getSurfaceTextFromTextTokens(text_tokens).split()
##	text=removeStopWords(text)
##	for t in text:
##		words[t]+=1
##		pl=makeSingularPlural(t)
##		if pl != "" and pl != t:
##			words[pl]+=1

	scores=[]
	for tp in classes:
		score=0.0
		for word in tp[1]:
			score+=words[word]
		score/=float(tp[2]) # normalise the score by the word length
		scores.append((tp[0],score))

	# Should it add the yago: category of the word with the highest TF?
	if ADD_ROOT_CATEGORY:
		if len(words)>0:
			best="http://dbpedia.org/class/yago/"+sorted(words.iteritems(),key=lambda x:x[1],reverse=True)[0][0]
			if best not in [s[0] for s in scores]:
				scores.append((best,100))

	scores=sorted(scores,key=lambda x:x[1],reverse=True)
	return scores

def getSentenceTokenDensity(sp):
	return len([x for x in sp if isinstance(x,templateSlot)]) / float(len(sp))

def getTriplesForLinkedEntities(subj,triples,tokens):
	"""
	in -> subj = string ("$self" most often), triples = list of rdfTriple, tokens= (work,link)
	out -> list of rdfTriple retrieved, for each entity link spotted in the links of the tokens
	"""
	res=[]
	all_uris=[(t.value,t.pred) for t in triples if t.value.startswith("http://")]
	entities=spotLinkedEntitiesInText(all_uris,tokens)

	for e in entities:
		link=extractClassName(e[0])
		res.extend(selectTriplesLanguage(getTriplesUsingCache(link,buildGraphPath([subj,e[1]])),"EN"))
	return res

def buildContentNgrams(model,atlist):
	"""
	Increase the counts of the ngrams for a model by counting ngrams in the present
	article, given as an alignedToken list
	"""
	ngramlist=[[""],[""],[""]]

	for t in atlist:
		if len(t.spottedTriples) > 0:
			ngramlist.append(t.spottedTriples)

	for x,n in enumerate(ngramlist[3:]):
		for i in range(1,maxNgram+1):
			y=x+3+1
			nglist=iterateBuildNgrams(ngramlist[y-i:y],0)
			for ng in nglist:
				ng=tuple(ng)
				model.ngrams[i][ng]+=1

##	print model.ngrams
	return model

def countCo_oc(model,tokens):
	"""
	in -> model = classModel, tokens = list of template tokens
	"""
	for t in tokens:
		if isinstance(t,templateSlot):
			for i,e in enumerate(t):
##				model.co_oc_count[e]+=1
##				model.co_co_counts[-1][e]+=1
##				for t in triples:
##				if e != "" and t.pred not in [""]:
##				cm.co_oc_counts[-1][self_pointer+gp_sep+t.pred]+=1
				model.co_oc_counts[-1][e]+=1
				for k,f in enumerate(t):
					if e!=f:
						cd=model.co_oc[e][f]=model.co_oc[e].get(f,0)+1

def predScore(model,pred):
	return model.ngrams[1][pred]

def setWithAandB(sets,a,b):
	"""
	True if a list/set contains both a and b, False otherwise
	"""
	for s in sets:
		if a in s and b in s:
			return True
	return False

def computePredicateEquivalence(model):
	"""
	builds the equivalence dict (model.co_oc[e][f]) and the equivalence classes (model.pools)
	of predicates
	"""
	for e in model.co_oc:
		for f in model.co_oc[e]:
			separate=0
			for article in model.co_oc_counts:
				if e in article and f in article:
					separate+=article[e]
					separate+=article[f]
##			diff=abs(model.co_oc_count[e]-model.co_oc_count[f])
##			dice_c=(float(model.co_oc[e][f]) + diff) / float((model.co_oc_count[e]+model.co_oc_count[f])/2)
			dice_c=float(model.co_oc[e][f]) / float(separate / 2)
			model.dice_coefs[(e,f)]=dice_c
			model.dice_coefs[(f,e)]=dice_c

			if dice_c >= DICE_COEFFICIENT:
				model.equivalent[e].append(f)
	sets=[]

	for e in model.equivalent:
		for f in model.equivalent[e]:
			if len(sets) == 0:
				sets.append([e,f])
				continue
			if setWithAandB(sets,e,f):
				continue
			for s in sets:
				if f in s:
					if e in s:
						break
##						break_again=True
					else:
						s.append(e)
						break
				else:
					if e in s:
						s.append(f)
						break
			else: # if no break has happened in the loop
				sets.append([e,f])

	model.pools=[]
	for s in sets:
		s=list(set(s))
		s=sorted(s,key=lambda x:predScore(model,x),reverse=True)
		pool=predPool()
		pool.extend(s)
		model.pools.append(pool)

##	if model.name=="http://dbpedia.org/class/yago/GermanComposers":
##		assert 1==0
	for pred in model.ngrams[1]:
		if pred[0] != '$':
			found=False
			for p in model.pools:
				if pred[0] in p:
					found=True
					break
			if not found:
				p=predPool()
				p.append(pred[0])
				model.pools.append(p)

def subsMetaPredicates(model):
	"""
	For each slot in each sentence extracted, substitute all equivalent GraphPaths
	for only the top one from model.pools
	"""
	for s in model.sentences:
		for t in s:
			if isinstance(t,templateSlot):
				newlist=[]
				for s in model.pools:
					for p in t:
						if p in s:
							newlist.append(s[0])
				newlist=list(set(newlist))
				if len(newlist) > 0:
					for cnt in range(len(t)): t.pop() # remove all elements
					t.extend(newlist) # add only the top GP from each pool

def adjustNgramCounts(model):
	"""
	Divide ngram counts by N of ngrams of that len, get probabilities
	"""
	for n in range(1,maxNgram):
		for ng in model.ngrams[n]:
			model.ngrams[n][ng]=model.ngrams[n][ng]/float(len(model.ngrams[n]))

def adjustPoolsCount(model):
	"""
	Compute the number a times a predicate pool can be issued per article
	"""
	for p in model.pools:
		p.count_per_article=max(1,round(model.ngrams[1][(p[0],)]))


def shortest_path():
	"""
	choose the shortest GP of the options
	"""
	pass

def moreThanOneRefFromSubTriples(template):
	pass

def subsTemplateWithSubTriples(template):
	pass

def templatesAreEqual(t1,t2):
	"""
	compares 2 sentence templates, element-by-element

	in -> t1, t2 = sentence templates (lists)
	out-> True if they are identical or equivalent (at least one shared gp in each slot)
		  False otherwise
	"""
	if len(t1) != len(t2):
		return False
	for i,t in enumerate(t1):
		if isinstance(t,basestring):
			if t != t2[i]:
				return False
		if isinstance(t, list):
			if not isinstance(t2[i],list):
				return False
			found=False
			if len(t) != len(t2[i]):
				return False
			for f,gp in enumerate(t):
				if gp == t2[i][f]:
					found=True
			if not found:
				return False
	return True

def findTemplate(templates,t):
	"""
	Return the index of a template in the sentence list
	if not found, return -1
	"""
	for i,t2 in enumerate(templates):
		if templatesAreEqual(t,t2):
			return i
	return -1

def subsVarNodesInSentence(s):
	"""
	Put brackets around the spotted text chunks in a sentence
	"""
	res=""
	for token in s:
		if len(token.spottedTriples) > 0:
			res+="["+token.text+"]"
		else:
			res+=token.text
		res+=" "
	return res

def addTemplateOrIncreaseCount(cm,new_template):
	"""
	Add a template to the model or increase its count if it's already there
	"""
	match=findTemplate(cm.sentences,new_template)
	if match != -1:
		cm.sentences[match].count+=1
	else:
		new_template.count=1
		cm.sentences.append(new_template)


def processArticle(wiki_link,manual_class=""):
	"""
 	Main procedure for training: call for each wikipedia article

	"""
	current_models=[]
	tokens=getArticleTokens(wiki_link)  # return list of tokens (word,link).

	if tokens == []:
		print wiki_link,": article not in SQLite!"
		return False
	print "Loading article:",wiki_link," - ",

	triples=filterOutUselessTriples(getTriplesUsingCache(wiki_link,self_pointer))
	if len(triples)==0:
		print "No triples found in SPARQL - aborting"
		return False

	stats.total_articles+=1

##	linkedEntityTriples=getTriplesForLinkedEntities(self_pointer,triples,tokens)
##	triples.extend(linkedEntityTriples)

	sltokens=generateSLtokens(triples)
##	triples=shortenTripleNamespaces(triples)

##	sentences=[]
##	for s in loaded_sentences:
##		new_sent=clusterMultiWordValues(s,listMultiWordValues(triples))
##		sentences.append(new_sent)

	# cluster values for NER, tag phrase breaks
	tokens=clusterMultiWordValues(tokens,listMultiWordValuesFromSL(sltokens))
	tokens=tagPhraseBreaks(tokens)

	# choose article class
	if manual_class=="":
		class_list=listArticleTriplesByPred(rdf_type,triples)
##		print "\n1. All classes:",class_list
		article_classes=orderClassesByTFwithText(class_list,joinLists(extractSentencesFromTokens(tokens,2)))
		article_classes=[c[0] for c in article_classes][:MAX_CLASSES]
		print "\n Best classes from triples and text:"
		for c in article_classes: print c

		article_classes=list(set(article_classes))

		for ac in article_classes:
			current_models.append(classModels.setdefault(ac,classModel(ac)))

	else:
		article_class=manual_class
		current_models.append(classModels.setdefault(article_class,classModel(article_class)))

	# NER / spotting: spot alignments
	atlist=spotAlignmentsAsATlist(sltokens,tokens)
	sents=extractSentencesFromATlist(atlist,max_rank)

	stats.total_sentences+=len(sents)

	for cm in current_models:
		buildContentNgrams(cm,joinLists(sents))
		cm.article_count+=1

		cm.co_oc_counts.append(defaultdict(int))


	# coreference resolution
	self_surface=generateSLfromLink(wiki_link) # generate list of standard surface realisations of $self
	sents,pronoun=annotateSimpleSelfCoreference(sents,self_surface)

	# store the "he"
	for cm in current_models:
		cm.pronouns[pronoun]+=1

	sents=selectSentencesByCoreference(sents,self_pointer)

	for s in sents:
		s_string=" ".join([t.text for t in s])
		stats.total_sentences+=1

		num_slots=slotsWithInfo(s)
		if num_slots < 2:
			continue

		stats.sent_considered.append(s_string)

		# if running the tree parse
		if PRUNE_PARSE:
			parse,created_vars=createParseFromATList(s)
			tree2,dep=treeDepFromStanfordParse(parse)
	##		print [(rel, gov.text, dp.text) for rel, gov, dp in dep.dependencies]
			tree=copy.deepcopy(tree2)
			tree,pruned=pruneTreeCFG(tree,dep)

			tree3=copy.deepcopy(tree)
			tree3=subsVarNodesInTree(tree3,created_vars)
			tree2=subsVarNodesInTree(tree2,created_vars)
##			draw_trees(tree,tree2)
##			fancyDrawTrees(tree,tree2)

			if len(tree) > 0:
				s=restoreATlistFromTree(tree,created_vars)
			else:
				print "No sentence extracted from",s
##				stats.sent_discarded_during_parse.append(s_string)
				stats.logSentenceOutcome(s_string,num_slots,FO_DISCARDED_DURING_PARSE,tree2,tree)
				continue

			num_slots=slotsWithInfo(s)
			if num_slots < 1:
				print "Fewer than 1 slots in:",s_string
	##			print createSentenceTemplateFromATlist(s).toStringTemplate()
##				stats.sent_discarded_after_parse.append(s_string)
				stats.logSentenceOutcome(s_string,num_slots,FO_DISCARDED_AFTER_PARSE,tree2,tree)
				continue

		new_template=createNewSentenceTemplateFromATlist(s)

		for cm in current_models:
			countCo_oc(cm,new_template) # count co-occurrences of predicates

		density=getSentenceTokenDensity(new_template)
		str_template=new_template.toStringTemplate()
		print "Density:",density,"Sentence:",str_template

##		stats.templates_extracted.append(new_template.toStringTemplate())
		if not PRUNE_PARSE: tree="-";tree2="-"
		stats.logSentenceOutcome(s_string,num_slots,FO_TEMPLATE_EXTRACTED,tree2,tree,str_template)

		for cm in current_models:
			addTemplateOrIncreaseCount(cm,new_template)

	return True

def processArticleListFromFile(filename, manual_class="", num=0):
	"""
	Process a list of articles which it reads from a file given as parameter.

	manual_class=override the automatic choice of classes the article belongs to,
	set it manually

	num=process this many out of that list
	"""
	successful=[]
##	from time import sleep
	f=codecs.open(filename,"r",encoding="utf-8")

	cnt=0

	for line in f :
##		sleep(1)
		cnt+=1
		if num > 0 and cnt >= num:
			break

		line=line.strip("\n\r ")
		line=wikifyTitle(line)
##		print line

		if processArticle(line, manual_class):
			successful.append(line)

	f.close()
	f2=codecs.open("successful.txt","w",encoding="utf-8")
	for s in successful:
		f2.write(s+"\n")
	f2.close()

def walkThroughArticles(startpos=3500):
	"""
	Process all articles in SQLite database 1 by 1, start at startpos
	"""
	for article in sorted(wiki_articles,reverse=False)[startpos:]:
		if not article.startswith("List_of_"):
			processArticle(article)

def saveModelDict(models,filename):
	"""
	Pickle the trained model to a file
	"""
	import cPickle
	print "Saving class models to file: ", filename
	f=open(filename,"w")
	cPickle.dump(models,f)
	f.close()


def saveStatsToFile(filename):
	f=codecs.open(filename,"w",encoding="utf-8")
	f.write("Total number of articles: %d\n" % stats.total_articles)
	f.write("Total number of sentences: %d\n" % stats.total_sentences)
	f.write("Number of sentences considered: %d\n" % len(stats.sent_considered))
	f.write("Number of sentences discarded during parse: %d\n" % len(stats.sent_discarded_during_parse))
	f.write("Number of sentences discarded after parse: %d\n" % len(stats.sent_discarded_after_parse))

	f.write("\n\n\n\n ===== SENTENCES CONSIDERED =====")
	for s in stats.sent_considered:f.write(s+"\n")
	f.write("\n\n ===== SENTENCES DISCARDED DURING PARSE =====")
	for s in stats.sent_discarded_during_parse:f.write(s+"\n")
	f.write("\n\n\n\n ===== SENTENCES DISCARDED AFTER PARSE =====")
	for s in stats.sent_discarded_after_parse:f.write(s+"\n")
	f.close()

def trainModelForClass(training_class,filename,max_num=1000):
	"""
	Given a class (e.g. "http://dbpedia.org/class/yago/GermanComposers"), retrieve
	from SPARQL endpoint all entities of that class, and for each of them with entries in
	the SQLite database, train on those articles
	"""
	q="""
	SELECT ?subj WHERE {
	?subj a <%s>}
	""" % training_class
##	sparqlQuery(q,default_sparql_endpoint)

	data=""
	while data=="":
		data=sparqlQuery(q, default_sparql_endpoint)

	if isinstance(data,str):
		failcount=1
		print "SPARQL error!\n Result: ",data
		while isinstance(data,str) and failcount < 3:
			print "Trying again..."
			failcount+=1
			data=sparqlQuery(q, "http://dbpedia.org/sparql/")
		raise Exception

	found_entities=[]

	for d in data.items()[1][1]['bindings']:
##		print d.items()[1][1]["value"], " = ", d.items()[2][1]["value"]
		subj=d.items()[0][1]["value"]
		found_entities.append(extractClassName(subj))

	total_done=0
	for wiki_link in found_entities:
##		print wiki_link
		if wiki_link in wiki_articles:
			processArticle(wiki_link)
			total_done+=1
			if total_done >= max_num:
				break

	postProcessModels()
	saveModelDict(classModels,filename)
##	pprintTriplesFor(classModels,found_entities[0])

def purgeSentencesWithConflictingPredicates(model):
	"""
	Removes ambiguous sentences from the model.
	"""
	to_remove=[]
	for s in model.sentences:
		for token in s:
			if isinstance(token, templateSlot):
				for e in token:
					for f in token:
						if e!=f:
							if model.dice_coefs[(e,f)] < DICE_PURGE:
								to_remove.append(s)
	for r in to_remove:
		print "Purging sentence:",r
		if model.sentences.index(r) != -1:
			model.sentences.remove(r)

def	postProcessModels():
	"""
	Call this after all the desired articles have been processed.

	It joins predicates that consistently have the same values in predicate "pools",
	substitutes multiple predicates in the slots in the templates by only the "best" of them,
	divides ngram counts to get ngram statistics, computes the numer of times every "pool" can be
	instantiated per article
	"""
	for model in classModels.itervalues():
		computePredicateEquivalence(model)
		subsMetaPredicates(model)
		adjustNgramCounts(model)
		adjustPoolsCount(model)
		purgeSentencesWithConflictingPredicates(model)
		if model.name=="http://dbpedia.org/class/yago/GermanComposers":
			print "\n",model.name
			for p in model.pools:
				print p
##			assert 1==0


def main():
	"""
	main: run to train the model
	"""

	if PRUNE_PARSE: init_parser()
	model_filename="c:\\nlp\\thesis\\bl_model2.mod"
##	processArticleListFromFile("german_composers.txt","http://dbpedia.org/class/yago/GermanComposers",num=15)
##	processArticleListFromFile("german_composers.txt",num=5)
##	processArticleListFromFile("countries.txt","http://dbpedia.org/class/yago/Countries",num=20)
###	walkThroughArticles()
##	processArticle("Diego_Maradona")
##	processArticle("Metallica")
##	processArticle("Mercyful_Fate")



##
##	print model
##sorted(classModels["http://dbpedia.org/class/yago/GermanComposers"].ngrams[3].iteritems(),key=lambda x:x[1],reverse=True)[:10]

	trainModelForClass("http://dbpedia.org/class/yago/LandlockedCountries",model_filename,10)

##	trainModelForClass("http://dbpedia.org/class/yago/GermanComposers",model_filename)
##	trainModelForClass("http://dbpedia.org/class/yago/SpeedMetalMusicalGroups",model_filename)

##	postProcessModels()
##	assert 1==0
##	stats.writeToCSV("c:\\nlp\\thesis\\bl_model1.csv")
##	saveStatsToFile("stats.txt")

##	saveModelDict(classModels,model_filename)

if __name__ == '__main__':
	main()

