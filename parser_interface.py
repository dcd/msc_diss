#-------------------------------------------------------------------------------
# Name:		parser_interface
# Purpose:
#
# Author:	  Daniel Duma
#
# Created:	 03/07/2012
# Copyright:   (c) Daniel Duma 2012
# Licence:	 <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from stanfordparser import Parser

##from nltk.tree import Tree
from nltk.tree import *
from nltk.draw.tree import draw_trees
import re

from util import *

SFparser=None

remove_pos=["JJ","JJS","JJR","NN","NNP","NNS","RBS", "RB", "RBR","CD"] # POS tags that require evidence in data
conserve_words=["one","are","size","population"] # NN that don't require evidence (for spotting quantities)
syntax_dependency={"VB":"VP","VP":"S","SBAR":"S","JJ":"ADJP","NN":"NP","NP":"NP"}
requires_child={"VP":["VB","VP"],"ADJP":["JJ"],"NP":["NN","NP","CD","LST","LS"],"S":["VP","SBAR"],"PP":["ADJP","NP"]}
requires_sister={"DT":["ADJP","NP","NN"]}
requires_siblings=["VP","IN","TO","OF"]
requires_right_mod=["WDT","CC"]
ignore_siblings=[".",",",":",";"]
cant_end_sentence_on_this=["CC", "VB", "WHNP"]

verbose_pruning=False

def treeFromStanfordParse(s):
	"""
	Parse the sentence s using the Stanford parser, return an NLTK tree
	"""
	tokens,tree=SFparser.parse(s)
	ts=tree.toString()

	ts=re.sub(r"\[.*?\]","",ts)
	nt=Tree(ts)

	return nt

def treeDepFromStanfordParse(s):
	"""
	Parse the sentence s using the Stanford parser, return an NLTK tree and
	a list of dependencies
	"""
	dep,tree=SFparser.parseToStanfordDependencies(s)
	ts=tree.toString()

	ts=re.sub(r"\[.*?\]","",ts)

	nt=Tree(ts)

	return nt,dep

def createParseFromATList(sentence):
	"""
	Return a string where the spotted entities are replaced by "var_X" placeholders

	(dirty hack to avoid using textStandoff objects)
	"""
	varnum=1
	res=u""
	created_vars={}
	for token in sentence:
		if len(token.spottedTriples) > 0:
			if token.text[0].isupper():
				vn="Var_%s" % varnum
			else:
				vn="var_%s" % varnum

			res+=vn+" "
			created_vars[vn.lower()]=token
			varnum+=1
		else:
			res+=token.text+" "
	res=res.strip()
	res=res[0].upper()+res[1:]
	res+="."

	return res,created_vars

def restoreATlistFromTree(tree,created_vars):
	"""
	Create a list of alignedToken instances from a tree and a list of placeholder
	variables
	"""
	atlist=[]
	for l in tree.leaves():
		if l.lower().startswith("var_"):
			atlist.append(created_vars[l.lower()])
		else:
			atlist.append(alignedToken(l))

	return atlist

def subsVarNodesInTree(tree,created_vars):
	"""
	Put square brackets around the spotted entities, substitute placeholder for
	actual string
	"""
	for p in tree.treepositions():
		if isinstance(tree[p],basestring):
			if tree[p].lower().startswith("var_"):
				tree[p]="["+created_vars[tree[p].lower()].text+"]"
	return tree

def sisterNodes(tree,treepos):
	"""
	Return a list of the nodes hanging from the same parent as tree[treepos],
	excluding that node
	"""
	parent=treepos[:-1]
	sisters=[]
	for c in tree[parent]:
		try:
			if c != tree[treepos]:
				sisters.append(c)
		except:
			pass
	return sisters

def parentNode(tree,treepos):
	return tree[treepos[:-1]]

def findNode(tree,nodelabel):
	"""
	return the index of the first node with a label (can be a RE) that matches nodelabel,
	-1 if not found
	"""
	for i,node in enumerate(tree):
		if isinstance(node,Tree) and re.match(nodelabel,node.node,re.IGNORECASE):
			return i
	return -1

def sisterOnTheRight(tree,p):
	"""
	True if tree[p] has a sister node on the right, False otherwise
	"""
	parent=tree[p[:-1]]
	index=parent.index(tree[p])

	if index != -1 and (index < len(parent)-1):
		for node in parent[index+1:]:
			if node.node not in ignore_siblings:
				return True
	return False

def getSisterOnTheRight(tree,p):
	"""
	True if tree[p] has a sister node on the right, False otherwise
	"""
	parent=tree[p[:-1]]
	index=parent.index(tree[p])

	if index != -1 and (index < len(parent)-1):
		return parent[index+1:index+2][0]
	return None

def pruneTreeCFG(tree,dep):
	"""
	The prune tree function.

	tree=NLTK.Tree
	dep=tuples with dependencies (not used for now)
	"""
	pruned=0
##		positions=[p for p in tree.treepositions() if len(p) > 2 and isinstance(tree[p],Tree) and tree[p].height() > 2]
	positions=[p for p in tree.treepositions() if len(p) >= 2 and isinstance(tree[p],Tree) and tree[p].node.endswith("P") and tree[p].height() == 3 ]

	for p in positions:
		to_remove=[]
		for i,l in enumerate(tree[p]):

			pos=list(p)
			pos.append(i)

			# if the node is a preterminal = it has a child that is not a tree
			if len(l) == 1 and not isinstance(l[0],Tree):
				prod=l[0]
				if not prod.lower().startswith("var_"):
					if prod[0].isupper():
						to_remove.append((l,pos))
					else:
						# if it's not a spotted entity or a word-to-keep
						if l.node in remove_pos and prod not in conserve_words:
							to_remove.append((l,pos))
			else:
				to_remove.append((l,pos))

			for rnode,rpos in to_remove:
				try:
					if verbose_pruning:print "1 Removing",rnode
					tree[p].remove(rnode)
					pruned+=1
				except:
					pass

	# phase 2:
	to_remove=[]
	for p in [p for p in tree.treepositions() if isinstance(tree[p],Tree)]:
		# childless nodes
		if len(tree[p])==0:
			if verbose_pruning:	print tree[p],"has length 0"
			to_remove.append((tree[p],p))
			continue
		# commas followed by commas
		if tree[p].node==",":
			sis=getSisterOnTheRight(tree,p)
			if sis is not None and sis.node==",":
				to_remove.append((tree[p],p))
				continue

# remove VPs with no complements
#   WHY U NO WORK?!!
		if tree[p].node in ["VP","VBD"]:
			support=True
			if not sisterOnTheRight(tree,p):
				support=False
				vb=findNode(tree[p],"VB\w+")
				if vb != -1:
					p2=list(p)
					p2.append(vb)
					if sisterOnTheRight(tree,p2):
						support=True
			if not support:
				to_remove.append((tree[p],p))
				continue

		# nodes that can't be left without sisters
		for head in requires_siblings:
			if tree[p].node.startswith(head):
				parent=parentNode(tree,p)
				has_complement=False
				for sister in sisterNodes(tree,p):
					if sister.node not in ignore_siblings:
						has_complement=True
				if not has_complement:
					to_remove.append((tree[p],p))
				continue

		# nodes that require support from specific children
		for head in requires_child:
			if tree[p].node==head:
				child_support=False
				sister_support=True
				for child in tree[p]:
					for supporting_pos in requires_child[head]:
						if child.node.startswith(supporting_pos):
							child_support=True
							break

				if child_support and (head in requires_sister):
					# nodes that require specific sisters
					sister_support=False
					for sister in sisterNodes(tree,p):
						for supporting_pos in requires_sister[head]:
							if sister.node.startswith(supporting_pos):
								support=True

				if not child_support or not sister_support:
					try:
						if verbose_pruning:print unicode(tree[p]),"doesn't have required child or sister"
					except:
						pass
					to_remove.append((tree[p],p))

##				break # save time by stopping when head found
	if len(positions) > 0 and tree[positions[-1]].node in cant_end_sentence_on_this:
		to_remove.append((tree[positions[-1]],positions[-1]))

	for rnode,rpos in to_remove:
		if verbose_pruning:print "2 Removing",unicode(rnode)
		try:
			tree[rpos[:-1]].remove(rnode)
			pruned+=1
		except:
			pass


	if pruned > 0:
		tree,pruned=pruneTreeCFG(tree,dep)

	return tree,pruned


def fancyDrawTrees(tree1,tree2):
	from nltk.draw.util import CanvasFrame, BoxWidget, OvalWidget, ParenWidget, TextWidget
	from nltk.draw.tree import TreeWidget

	tree1=tree1[(0,)]
	tree2=tree2[(0,)]

	def fill(cw):
		cw['fill'] = '#%06d' % random.randint(0,999999)

	def boxit(canvas, text):
		big = ('helvetica', -16, 'bold')
		return BoxWidget(canvas, TextWidget(canvas, text,
											font=big), fill='green')
	def ovalit(canvas, text):
		return OvalWidget(canvas, TextWidget(canvas, text),
						  fill='cyan')
	def color(node):
		node['color'] = '#%04d00' % random.randint(0,9999)
	def color2(treeseg):
		treeseg.node()['fill'] = '#%06d' % random.randint(0,9999)
		treeseg.node().child()['color'] = 'white'

	cf = CanvasFrame(width=550, height=450, closeenough=2, background="#FFFFFF")

	def useNiceFonts(tree):
		return TreeWidget(cf.canvas(), tree, shapeable=1,
					node_font=('helvetica', -14, 'bold'),
					leaf_font=('helvetica', -14, 'bold'),
					roof_fill='white', roof_color='black',
					leaf_color='green4', node_color='blue2')

	tc = useNiceFonts(tree1)
	cf.add_widget(tc,10,10)

	tc2 = useNiceFonts(tree2)

	tc.bind_click_trees(tc.toggle_collapsed)
	tc2.bind_click_trees(tc2.toggle_collapsed)
	tc.bind_click_nodes(color, 3)
	tc2.expanded_tree(1).bind_click(color2, 3)
	tc2.expanded_tree().bind_click(color2, 3)

	paren = ParenWidget(cf.canvas(), tc2)
	cf.add_widget(paren, tc.bbox()[2]+10, 10)

##	textbox = BoxWidget(cf.canvas(), twidget, fill='white', draggable=1)

	# Run mainloop
	cf.mainloop()

def init_parser():
	"""
	Call this to initialise the library!
	"""
	global SFparser
	SFparser = Parser()