#-------------------------------------------------------------------------------
# Name:        module2
# Purpose:
#
# Author:      MasterMan
#
# Created:     18/03/2012
# Copyright:   (c) MasterMan 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

import urllib, json


class ontoInfo:
    def __init__(self, ontoclass):
        self.ontoclass=ontoclass
        self.predicates=[]

class predInfo:
    def __init__(self, pred):
        self.pred=pred
        self.position=[]



# HTTP URL is constructed accordingly with JSON query results format in mind.
def sparqlQuery(query, baseURL, format="application/json"):
    params={
    "default-graph": "",
    "should-sponge": "soft",
    "query": query,
    "debug": "on",
    "timeout": "",
    "format": format,
    "save": "display",
    "fname": ""
    }
    querypart=urllib.urlencode(params)
    response = urllib.urlopen(baseURL,querypart).read()
    res=response
    try:
        res=json.loads(response)
    except:
        pass
    return res



cache={}
stats={}

def updateStats(ontoclass, pred, inc_count,position):
    cl=stats.setdefault(ontoclass,{})



def cachedRetrieval(wiki_link):
    if wiki_link in cache:
        return cache[wiki_link]
    else:
        return getInfo(wiki_link)
    pass

def getInfo(wiki_link):
    keys=[]
    values=[]
    langs=[]

    query="""
PREFIX foaf: <http://xmlns.com/foaf/0.1/>

SELECT ?dbpedia_link, ?attr, ?val
 WHERE {
 ?dbpedia_link foaf:page <http://en.wikipedia.org/wiki/%s> .
 ?dbpedia_link ?attr ?val
}
 """ % wiki_link

    data=sparqlQuery(query, "http://dbpedia.org/sparql/")

    for d in data.items()[1][1]['bindings']:
##        print d.items()[1][1]["value"], " = ", d.items()[2][1]["value"]
        pred=d.items()[1][1]
        obj=d.items()[2][1]
        if obj["type"]=="literal":
            lng=obj["xml:lang"]
        else:
            lng="uri"

        keys.append(pred["value"])
        values.append(obj["value"])
        langs.append(lng)

##    print json.dumps(data)

    return keys,values,langs



def predicateList(pred,keys,values):
    res=[]
    for i,p in enumerate(keys):
        if p==pred:
            res.append(values[i])
    return res

keys, values, langs = getInfo("Johannes_Brahms")

ignore_predicates = ["http://dbpedia.org/ontology/abstract","http://www.w3.org/2000/01/rdf-schema#comment"]

##for i,k in enumerate (keys):
##    if keys[i] not in ignore_predicates:
##        print keys[i], " = ", values [i]

##print predicateList("http://www.w3.org/2000/01/rdf-schema#label")
print predicateList("http://www.w3.org/1999/02/22-rdf-syntax-ns#type", keys, values)


