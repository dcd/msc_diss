#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      MasterMan
#
# Created:     04/07/2012
# Copyright:   (c) MasterMan 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from bl_train import *
from util import *
from wiki import *
from alignments import *

import sys

successful=[]

def testParser(wiki_link):

	pass

def testTokenizer(wiki_link,outfile):
	text=loadArticleFromDB(wiki_link)

	if text=="":
		print "Article not in SQLite!"
		return

	successful.append(wiki_link)

	text=normalizeCharacters(text)
	sentences=sentence_tokenizer.tokenize(text)
	for s in sentences:
		outfile.write(sent+"\n\n")


def testSpotting(wiki_link,outfile):
	tokens=getArticleTokens(wiki_link)  # return list of tokens (word,link).

	if tokens == []:
		print wiki_link,": article not in SQLite!"
		return False
	print "Loading article:",wiki_link," - ",

	triples=selectTriplesLanguage(getTriplesUsingCache(wiki_link,self_pointer),"EN")
	if len(triples)==0:
		print "No triples to spot - aborting"
		return False

##	linkedEntityTriples=getTriplesForLinkedEntities(self_pointer,triples,tokens)
##	triples.extend(linkedEntityTriples)

	triples=shortenTripleNamespaces(triples)

	sltokens=generateSLtokens(triples)
	triples=shortenTripleNamespaces(triples)

 	tokens=clusterMultiWordValues(tokens,listMultiWordValuesFromSL(sltokens))
	tokens=tagPhraseBreaks(tokens)

	# NER / spotting: spot alignments
	atlist=spotAlignmentsAsATlist(sltokens,tokens)
	sents=extractSentencesFromATlist(atlist,max_rank)

	successful.append(wiki_link)

	outfile=codecs.open(outfile,"w","utf-8")
	for s in sents:
		num_slots=slotsWithInfo(s)
		if num_slots < 2:
			continue

		s_text=subsVarNodesInSentence(s)
		outfile.write(s_text+"\n")

		for token in s:
			if token.hasTriples():
				outfile.write("["+token.text+"]=")
				for t in token.spottedTriples:
					outfile.write(t+", ")
                outfile.write("\n\n")

		outfile.write("\n\n")


def testArticleList(filename, outputfile, num=0):
	f=codecs.open(filename,"r",encoding="utf-8")
	f2=codecs.open(outputfile,"w",encoding="utf-8")

	cnt=0
	for line in f :
		cnt+=1
		if num > 0 and cnt >= num:
			break

		line=line.strip("\n\r ")
		line=wikifyTitle(line)
##		print line
		print "Processing article:",line

		try:
			testSpotting(line,f2)
##			successful.append(line)
			pass
		except: # catch *all* exceptions
			e = sys.exc_info()[0]
			print  "Exception: %s" % e

	f.close()
##	f2.close()
##	f2=codecs.open("successful.txt","w",encoding="utf-8")
##	for line in successful:
##		f2.write(line+"\n")
##	f2.close()

##	print "Testing sentence splitter"
##testArticleList("german_composers.txt","sentence_test.txt")
print "Testing NER spotting"
##testArticleList("german_composers.txt","spotting_test.txt")
##testArticleList("for_experiment_a.txt","spotting_test.txt")
testSpotting("Johann_Sebastian_Bach","spotting_test.txt")
