#-------------------------------------------------------------------------------
# Name:		coreference
# Purpose:
#
# Author:	  Daniel Duma
#
# Created:	 01/06/2012
# Copyright:   (c) Daniel Duma 2012
# Licence:
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from util import *

def annotateSimpleSelfCoreference(sentences,self_surface):
	if len(sentences) < 2:
		return sentences,""

	pronoun=""
	found_pronoun=False
	for s in sentences:
		for w in s:
##			for triple in w.spottedTriples:
			for pred in self_corefering_predicates:
				if buildGraphPath([self_pointer, pred]) in w.spottedTriples:
					w.addSurfaceFeature(token_nom)
					w.coref=self_pointer

			if w.text in self_surface:
				w.coref=self_pointer
				w.addSurfaceFeature(token_nom)

			if not found_pronoun:
				if w.text.lower() in pronouns:
					pronoun=w.text.lower()
					w.coref=self_pointer
					w.addSurfaceFeature(token_nom)
					found_pronoun=True
			else:
				if w.text.lower() == pronoun:
					w.coref=self_pointer
					w.addSurfaceFeature(token_nom)
				if w.text.lower() in posessives[pronoun]:
					w.coref=self_pointer
					w.addSurfaceFeature(token_gen)

	return sentences,pronoun
