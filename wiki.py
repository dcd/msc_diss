#-------------------------------------------------------------------------------
# Name:		wikim
# Purpose:
#
# Author:	  Daniel Duma
#
# Created:	 05/04/2012
# Copyright:   (c) Daniel Duma 2012
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from os.path import exists
import sqlite3
from util import *
import codecs
import nltk.data

class alignedSentence(list):
	pos=0

##article_dbpath="c:\\nlp\\thesis\\simplewiki_articles_test.db"
article_dbpath="c:\\nlp\\thesis\\simplewiki_articles_full.db"
wiki_articles=set()

url_regex=r"""^
(# Scheme
 [a-z][a-z0-9+\-.]*:
 (# Authority & path
  //
  ([a-z0-9\-._~%!$&'()*+,;=]+@)?              # User
  ([a-z0-9\-._~%]+                            # Named host
  |\[[a-f0-9:.]+\]                            # IPv6 host
  |\[v[a-f0-9][a-z0-9\-._~%!$&'()*+,;=:]+\])  # IPvFuture host
  (:[0-9]+)?                                  # Port
  (/[a-z0-9\-._~%!$&'()*+,;=:@]+)*/?          # Path
 |# Path without authority
  (/?[a-z0-9\-._~%!$&'()*+,;=:@]+(/[a-z0-9\-._~%!$&'()*+,;=:@]+)*/?)?
 )
|# Relative URL (no scheme or authority)
 ([a-z0-9\-._~%!$&'()*+,;=@]+(/[a-z0-9\-._~%!$&'()*+,;=:@]+)*/?  # Relative path
 |(/[a-z0-9\-._~%!$&'()*+,;=:@]+)+/?)                            # Absolute path
)
# Query
(\?[a-z0-9\-._~%!$&'()*+,;=:@/?]*)?
# Fragment
(\#[a-z0-9\-._~%!$&'()*+,;=:@/?]*)?
$"""


def tokenizeWikiMarkup(text):
	"""  tokenize text into tuples (word,uri)"""
##	if len(text) > 0 and text[0] in "]:":text=text[1:]
##	if text[1]=="]":text[0]=" "; text[1]=" "
##	text=text.replace("'''","\"")
##	text=text.replace("''","\"")
##	text=text.replace("\"","\"")
	text=text.replace(";"," ; ")
	text=text.replace("("," ( ")
	text=text.replace(")"," ) ")
##	text=text.replace("]],","]] , ")
	# add extra space between word.word
	text=re.sub(r"([^\d])([,.])([^\d])",r"\1 \2 \3",text)
	# remove stupid section headings
##	text=re.sub(r"=+\s*?.+?\s*?=+",r" ",text)

	# cluster headings with bullet points
	text2=re.sub(r"(={2,3})(.*?)(\1)(.\*.*?)(={2,3})",r"\1\2\3: \4 . \5",text)
	while text2 != text:
		text=text2[:]
	   	text2=re.sub(r"(={2,3})(.*?)(\1)(.\*.*?)(={2,3})",r"\1\2\3: \4 . \5",text)
	text=text2

	# remove urls
	text=re.sub(url_regex,r'<URL>',text)

	text=re.sub(r"rev>",r" ",text)
##	text=re.sub(r"\(",r" \( ",text)

	text=fuseTokens(text,"[[","]]")
	text=fuseTokens(text,"\"","\"")

	text=fuseTokens(text,"===","===")
	text=fuseTokens(text,"==","==")

##	text=re.sub("(=){3}.*(=){3}",text,"")

	tokens=[]
	words=text.split(" ")

	for w in words:
		if w=="":
			continue
		w2=w.replace("%%", " ")
		ibpos=w2.find("[[")
		if ibpos > -1:
			pipepos=w2.find("|",ibpos+2)
			if pipepos > -1:
				argpos=w2.find("]]",pipepos+2)
				if argpos > -1:
					label=w2[:ibpos]+w2[pipepos+1:argpos]+w2[argpos+2:]
				else:
					label=w2[:ibpos]+w2[pipepos+1:-2]

				link=w2[2:pipepos]
			else:
				label=w2.replace("[[","").replace("]]","")
				link=wikifyTitle(label)
		else:
			if w2.startswith("\""):
				label=w2[1:-1]
			else:
				label=w2
			link=""
		tokens.append((label,wikifyTitle(link)))

	return tokens


def loadArticleFromFile(filename):
	f=codecs.open(filename,"r",encoding="utf-8")
	lines=f.readlines()
	f.close()

	all_words=[]
	for l in lines:
		words=l.split()
		all_words.extend(words)

	return all_words

def fuseTokens(text,mk1,mk2):
	ibpos=text.find(mk1)
	while ibpos > -1:
		ibendpos=text.find(mk2,ibpos+len(mk1))
		if ibendpos == -1:
			ibendpos = len(text)

		if " " in text[ibpos+len(mk1):ibendpos]:
			insert=text[ibpos+len(mk1):ibendpos].replace(" ","%%")
			text=text[:ibpos+len(mk1)]+insert+text[ibendpos:]

		ibendpos=text.find(mk2,ibpos+len(mk1))
		if ibendpos == -1:
			ibpos=-1
		else:
			ibpos=text.find(mk1,ibendpos+len(mk2))

	text=text.strip()
	return text

def loadArticleFromDB(wiki_link):
	"""
	return the full text for a wiki_link in the SQLite DB
	"""
	if exists(article_dbpath):
		try:
			conn=sqlite3.connect(article_dbpath)
	##		conn.text_factory=str
			c=conn.cursor()
	##		c.execute("select * from articles where wiki_link =?",(codecs.encode(wiki_link,"utf-8"),))
			c.execute("select * from articles where wiki_link =?",(wiki_link,))
			row=c.fetchone()
			c.close()
			conn.close()
			if row is not None:
				return row[2]
			else:
				return u""
		except:
			return u""
	else:
		raise Exception
	pass


def getArticleTokens(wiki_link):
	""" Return a list of tokens for the article from SQLite DB
		token: tuple (word, uri)
	"""
	text=loadArticleFromDB(wiki_link)
	text=normalizeCharacters(normalizeDatesWithRegEx(text))
	return tokenizeWikiMarkup(text)
	pass

def getArticleSentences(wiki_link):
	"""
		return a list of list of tokens from SQLite
	"""

	text=normalizeCharacters(normalizeDatesWithRegEx(loadArticleFromDB(wiki_link)))
	sentences=sentence_tokenizer.tokenize(text)

	return [tokenizeWikiMarkup(s) for s in sentences]


def loadArticlesDB():
	"""
	populate the wiki_articles dict with all the wiki_links stored in the articles
	SQLite DB
	"""
	if exists(article_dbpath):
		conn=sqlite3.connect(article_dbpath)
		conn.text_factory=str
		c=conn.cursor()
		c.execute("select wiki_link from articles")
		rows=c.fetchall()
		for row in rows:
			wiki_articles.add(row[0])
		c.close()
		conn.close()

##text="""
##'''April''' is the fourth [[month]] of the [[year]]. It has 30 [[day]]s. The name April comes from that [[Latin]] word ''aperire'' which means "to open". This probably refers to growing [[plant]]s in spring. April begins on the same day of week as [[July]] in all years and also [[January]] in leap years.  April's [[flower]] is the [[Sweet Pea]]. Its [[birthstone]] is the [[diamond]]. The meaning of the diamond is innocence.  == April in poetry == [[poetry|Poets]] use ''April'' to mean the end of winter. For example: ''April showers bring [[May]] flowers.''  == Events in April == * [[April Fools' Day]] occurs on [[April 1]]. * [[Easter]] occurs on a [[week|Sunday]] between [[March 22]] and [[April 25]]. * [[Australia]] and [[New Zealand]] celebrate [[ANZAC Day]] on 25 April. [http://www.awm.gov.au/dawn/spirit/meaning.asp ANZAC] means Australian and New Zealand Army Corps, and began in 1915.
##"""

def extractSentencesFromTokens(text_tokens,num):
	sentences=[[]]
	count=0
	for token in text_tokens:

		if token[1]!=phrase_break_token:
			sentences[-1].append(token)
		else:
			count+=1
			if num > -1:
				if count>=num:
					return sentences
			sentences.append([])

	return sentences

def extractSentencesFromATlist(text_tokens,num=-1):
	new_sent=alignedSentence()
	new_sent.pos=1
	sentences=[new_sent]
	count=1
	for token in text_tokens:

		if token.link!=phrase_break_token:
			sentences[-1].append(token)
		else:
			count+=1
			if num > -1 and count >= num:
				return sentences
			new_sent=alignedSentence()
			new_sent.pos=count
			sentences.append(new_sent)

	return sentences

def selectSentencesByPredicate(sentences,must_have_list):
	res=[]
	for s in sentences:
		has_alignment=False
		for token in s:
			for pred in token.spottedTriples:
				if pred in must_have_list:
					has_alignment=True
					break
			if has_alignment :
				break
		if has_alignment:
			res.append(s)

	return res

def selectSentencesByCoreference(sentences,must_corefer_with_list):
	"""
in -> sentences = list of sentences with alignedTokens, must_corefer_with_list = list of strings
out -> list of sentences that have a token in them with an item in must_corefer_with_list as coref
	"""
	res=[]
	for s in sentences:
		has_alignment=False
		for token in s:
			if token.coref.lower() in must_corefer_with_list:
				has_alignment=True
				break
			if has_alignment :
				break
		if has_alignment:
			res.append(s)

	return res

def selectSentencesByCoreferenceOrTitle(sentences,must_corefer_with_list):
	"""
in -> sentences = list of sentences with alignedTokens, must_corefer_with_list = list of strings
out -> list of sentences that have a token in them with an item in must_corefer_with_list as coref
	"""
	res=[]
	for s in sentences:
		has_alignment=False
		for token in s:
			if token.coref.lower() in must_corefer_with_list:
				has_alignment=True
				break
			if token.text.find("==") != -1:
				has_alignment=True
			if has_alignment :
				break
		if has_alignment:
			res.append(s)

	return res


def listMultiWordValuesFromTriples(triples):
	"""
in -> list of triples
out -> list of unique strings, ordered by number of spaces in them, descending
	"""
	res=[]
	for t in triples:
		if " " in t.value:
			res.append(t.value)

	res=set(res)
	res=sorted(res,key=lambda x:x.count(" "),reverse=True)
	return res

def listMultiWordValuesFromSL(sltokens):
	"""
in -> list of triples
out -> list of unique strings, ordered by number of spaces in them, descending
	"""
	res=[]
	for t in sltokens:
		if " " in t[1]:
			res.append(t[1])

	res=set(res)
	res=sorted(res,key=lambda x:x.count(" "),reverse=True)
	return res


def listAlignmentsInSentence(sentence):
	"""
	in -> sentence = list of alignedToken
	out -> list of tuples (triple, token.text, index_in_sentence)
	"""
	res=[]
	for index,token in enumerate(sentence):
		if len(token.spottedTriples) > 0:
			for triple in token.spottedTriples:
				tup=(triple,token.text,index)
				res.append(tup)
	return res


def listPredicatesInAlignments(alignments):
	res=[]
	for al in alignments:
		res.append(al[0])
	res=set(res)
	return res

def clusterMultiWordValues(tokens,values):
	"""
in -> tokens = list of tuples (word,link), values = list of multi-word strings
out -> list of tokens = tuples (word,link), where multiple tokens are collapsed
		into single tokens where they match a multi-word value
	"""
	pos=0
	res=[]

	if len(values)>0:
		while pos < len(tokens):

			for v in values:
				words=v.split()
				chunk=tokens[pos:pos+len(words)]
				matchesTokens=False
				if len(words) == len(chunk):
					matchesTokens=True
					for cnt in range(len(words)):
						# UNICODE WARNING: DO SOMETHING ABOUT THIS
##						if not re.match(words[cnt],chunk[cnt][0],re.IGNORECASE):
##						if codecs.decode(words[cnt],"utf-8").lower() != codecs.decode(chunk[cnt][0],"utf-8").lower():
						if words[cnt].lower() != chunk[cnt][0].lower():
							matchesTokens=False
							break
				if matchesTokens:   # we found a match with the current multi-word value
					text=" ".join([f[0] for f in chunk])
					link=" ".join([f[1] for f in chunk])
					res.append((text,link))
					pos+=len(chunk) # move the pointer by the length of the match in tokens
					break   # break of the values loop, go to "while"
				pass

			if not matchesTokens:   # if loop over values is over, no match found, add the present token
				res.append(tokens[pos])
				pos+=1
	else:
		res=tokens

	return res

def getSurfaceTextFromTextTokens(tokens):
	res=" ".join([t[0] for t in tokens]).strip()
	res=re.sub(r"(\d+)\s+([.,])\s+(\d+)",r"\1\2\3",res)
	res=re.sub(r"([\w])\s+([,.])",r"\1\2",res)
	return res

def tokensToText(text_tokens):
	res=[]
	for t in text_tokens:
		res.append(t[0])
	if t[1] != "":
		res.append("["+t[1]+"]")
	return listToString(res)



# main

loadArticlesDB()
##import nltk
##sentence_tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
##print getArticleTokens("August")