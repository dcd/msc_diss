info=[("http://dbpedia.org/resource/Joe_Bloggs","http://www.w3.org/1999/02/22-rdf-syntax-ns#type","http://dbpedia.org/class/yago/GermanComposers","uri",""),
("http://dbpedia.org/resource/Joe_Bloggs","http://dbpedia.org/ontology/birthDate","1969-03-12","typed-literal",""),
("http://dbpedia.org/resource/Joe_Bloggs","http://dbpedia.org/ontology/deathDate","2012-05-23","typed-literal",""),
("http://dbpedia.org/resource/Joe_Bloggs","http://www.w3.org/2000/01/rdf-schema#label","Joe Bloggs","literal",""),
("http://dbpedia.org/resource/Joe_Bloggs","http://xmlns.com/foaf/0.1/name","Joe Bloggs","literal","en"),
("http://dbpedia.org/resource/Joe_Bloggs","http://purl.org/dc/elements/1.1/description","some guy I just made up","literal","en"),
("http://dbpedia.org/resource/Joe_Bloggs","http://dbpedia.org/ontology/birthPlace","Narnia","literal","en"),
("http://dbpedia.org/resource/Joe_Bloggs","http://dbpedia.org/ontology/deathPlace","Nowhereland","literal","en"),
("http://dbpedia.org/resource/Joe_Bloggs","http://xmlns.com/foaf/0.1/surname","Bloggs","literal","en"),
("http://dbpedia.org/resource/Joe_Bloggs","http://xmlns.com/foaf/0.1/givenName","Joe","literal","en"),
("http://dbpedia.org/resource/Joe_Bloggs","http://dbpedia.org/property/shortDescription","Made up guy","literal","en"),
("http://dbpedia.org/resource/Joe_Bloggs","http://dbpedia.org/property/placeOfBirth","Narnia","literal","en"),
("http://dbpedia.org/resource/Joe_Bloggs","http://dbpedia.org/property/placeOfDeath","Nowhereland","literal","en")

]


from rdf import *

alltriples=[]

for inf in info:
    k=rdfTriple()
    k.subj=inf[0]
    k.pred=inf[1]
    k.value=inf[2]
    k.dataType=inf[3]
    k.lang=inf[4]

##    saveTripleToDB(k)
    alltriples.append(k)

pickleTriplesToDB(alltriples,"Joe_Bloggs")

