import codecs

##from alignments import *
from wiki import *
from util import *

def extractLines(wiki_link):
    tokens=tokenizeWikiMarkup(loadArticleFromDB(wiki_link))  # return list of tokens (word,link).

    if tokens == []:
        print wiki_link,": article not in SQLite!"
        return ""
    print "Loading article:",wiki_link," - ",

    tokens=tagPhraseBreaks(tokens)
    sentences=joinLists(extractSentencesFromTokens(tokens,2))
    s=" ".join([x[0] for x in sentences]).strip()
    return s

def extractLinesProcessFile(filename, filename2, num=0):
    successful=[]
##    from time import sleep
    f=codecs.open(filename,"r",encoding="utf-8")
    f2=codecs.open(filename2,"w",encoding="utf-8")
    cnt=0

    for line in f:
##        sleep(1)
        cnt+=1
        if num > 0 and cnt >= num:
            break

        line=line.strip("\n\r ")
        line=wikifyTitle(line)
##        print line

        lines=extractLines(line)

        if len(lines) > 0:
            f2.write(lines)
            f2.write("\n")
            successful.append(line)

    f.close()
    f2.close()
    f2=codecs.open("successful.txt","w",encoding="utf-8")
    for s in successful:
        f2.write(s+"\n")
    f2.close()


extractLinesProcessFile("persons.txt","firstlines.txt")