def fillInTemplateText(template,self_link):
	"""
	deprecated? does this ever get called?
	"""
	iters=[]
	filled_template=[]

	self_labels=quickTripleQuery(self_link,self_REG_default)
	if len(self_labels) > 0:
		self_basic_RE=self_labels[0].value
	else:
		self_basic_RE=generateBasicSL(self_labels[0])

	strtemplate=template.toStringTemplate()
	num=0
	for token in template:
		if isinstance(token,list):
			it=iterFill()
			it.graphPaths.extend(token)
			num+=1
			it.varnum=num
			iters.append(it)

	for it in iters:
		for t in it.graphPaths:
			if graphPathTrailCorefers(t):
				it.values.append(t)
			else:
				it.values.extend([k.value for k in getGraphPathValue(t,self_link)])
		it.values=set(it.values)

		if len(it.values) == 0: # if failed to find a value, drop entire generation
			print "No value found for: ",it.graphPaths
			return []

	options=recursiveIterate(strtemplate,iters,0)

	return options